Locations = new Mongo.Collection('locations');
Notifications = new Mongo.Collection('notifications');
Lifts = new Mongo.Collection('lifts');
Ratings = new Mongo.Collection('ratings');
Payments = new Mongo.Collection('payments');
RideRequests = new Mongo.Collection('rideRequests');

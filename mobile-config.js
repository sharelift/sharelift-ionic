App.info({
  id: 'com.shareliftapp.sharelift',
  name: 'Sharelift Beta',
  description: 'An iOS app built with Meteor',
  version: '0.0.5'
  // buildNumber: '101' // has to be a number, for some reason greater than 100
});

App.icons({
  'iphone_2x': 'public/icons/Icon120x120.png',
  'iphone_3x': 'public/icons/Icon180x180.png',
  'android_ldpi': 'public/icons/Icon36x36.png',
  'android_mdpi': 'public/icons/Icon48x48.png',
  'android_hdpi': 'public/icons/Icon72x72.png',
  'android_xhdpi': 'public/icons/Icon96x96.png'
});

App.launchScreens({
  'iphone_2x': 'public/splash/640x960Splash.png',
  'iphone5': 'public/splash/640x1136Splash.png',
  'iphone6': 'public/splash/750x1334Splash.png',

  // Apparently these are the wrong sizes... so wtf
  // 'android_ldpi_portrait': 'public/splash/splash-200x320.png',
  // 'android_mdpi_portrait': 'public/splash/splash-320x480.png',
  // 'android_hdpi_portrait': 'public/splash/splash-480x800.png',
  // 'android_xhdpi_portrait': 'public/splash/splash-720x1280.png'
});

//Gives access to all links accessing data outside the app.
//WE NEED TO WHITELIST DOMAINS IN THE FUTURE instead of doing this:
App.accessRule('*');  //says client can use whatever it wants
App.accessRule('http://*');
App.accessRule('https://*');

App.accessRule('http://www.shareliftapp.com/*');
App.accessRule('https://*.googleapis.com/*');
App.accessRule('https://*.google.com/*');
App.accessRule('https://*.gstatic.com/*');
App.configurePlugin('plugin.google.maps', {
    'API_KEY_FOR_IOS': 'AIzaSyBjUTnBj0wLepey0FipdJgbo0Q7s_2fvHs'
});

App.accessRule('https://*.stripe.com/*');

offerLiftSchema = new SimpleSchema({

  liftDirection : {
    type: String
  },
  startEndLocation : {
    type: String
    //you'll need to modify this is you actually use this schema
  },
  endLocation : {
    type: String
  },
  destinationId : {
    type: Number
  },
  departureTime : {
    type: Date
  },
  returnTime : {
    type: Date
  },
  seats : {
    type: Number,
    label: "Open seats",
    min: 1,
    max: 7 // ?dfadsf
  }
});

pushNotifSchema = new SimpleSchema({
  notifType : {
    type: String
  },
  userId : {
    type : String
  },
  userName : {
    type : String
  }
});

//This schema doesn't work right now...

// multiPushNotifSchema = new SimpleSchema({
//   userIds : {
//     type : Array
//   },
//   notifText : {
//     type : String
//   }
// });

ratingSchema = new SimpleSchema({
  ratingType : {
    type: String
  },
  liftId : {
    type: String
  },
  riderId : {
    type: String
  },
  driverId : {
    type : String
  },
  rating1 : {
    type : Number
  },
  rating2 : {
    type : Number
  },
  rating3 : {
    type : Number
  },

});

Meteor.startup(function() {


  // IMPORTANT: Sets credentials for sending email from shareliftapp.com
  process.env.MAIL_URL = 'smtp://support@shareliftapp.com:sharelift@mail.shareliftapp.com:465';


  // Note: All previous fixture operations were removed from this file on April 10, 2016.
  // They weren't really working anyway, but reference gitlab.com if you need them for some silly reason.


//For testing settings.json environment variables:
  // console.log("Testing settings.json on the server:");
  // console.log(Meteor.settings.test);
  // console.log(Meteor.settings.private.test);
  // console.log(Meteor.settings.public.settingsTestPublic);


  Locations.remove({});


  //==========================Locations===============================

  Locations.insert({
    locationId: 1,
    destination: false,
    isAvailable: false,
    destinationName: "#getyourassintonature photo (leave town by 9:45 am)",
    street1: "",
    street2: "",
    city: "Bozeman",
    state: "MT",
    zip: "59715",
    latitude: 45.6321389,
    longitude: -111.5438611
  });

  Locations.insert({
    locationId: 2,
    destination: true,
    isAvailable: true,
    destinationName: "Big Sky Resort",
    street1: "50 Big Sky Resort Road",
    street2: "",
    city: "Big Sky",
    state: "MT",
    zip: "59716",
    latitude: 45.2865,
    longitude: -111.3999
  });

  Locations.insert({
    locationId: 3,
    destination: true,
    isAvailable: true,
    destinationName: "Bridger Bowl",
    street1: "",
    street2: "",
    city: "Bozeman",
    state: "MT",
    zip: "59715",
    latitude: 45.8188,
    longitude: -110.9058
  });

  Locations.insert({
    locationId: 4,
    destination: true,
    isAvailable: true,
    destinationName: "Brighton Ski Resort",
    street1: "8302 S Brighton Loop Rd",
    street2: "",
    city: "Brighton",
    state: "UT",
    zip: "84121",
    latitude: 40.598166,
    longitude: -111.5829
  });

  Locations.insert({
    locationId: 5,
    destination: false,
    isAvailable: false,
    destinationName: "Powder Mountain",
    street1: "6965 E Powder Mountain Rd.",
    street2: "",
    city: "Eden",
    state: "UT",
    zip: "84310",
    latitude: 33.952197,
    longitude: -84.7007918
  });

  Locations.insert({
    locationId: 6,
    destination: false,
    isAvailable: false,
    destinationName: "Snowbird",
    street1: "9600 Little Cottonwood Canyon Rd",
    street2: "",
    city: "Snowbird",
    state: "UT",
    zip: "84092",
    latitude: null,
    longitude: null
  });



 // Houston.add_collection(Meteor.users);


});

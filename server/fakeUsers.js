Meteor.startup(function(){

  //==========================Create fake lifts & users===============================
  // ---- THIS IS SERIOUS! DON'T EVER INCLUDE THIS CODE IN STAGING OR PRODCUTION!!!! ----

  // ---- Comment out if yo u don't want fake things -----


  //  Lifts.remove({isFake : true});
  //  Lifts.remove({}); // ALL LIFTS WILL GO AWAY! THIS IS SERIOUS
   //
  //  Notifications.remove({}); // ALL THE NOTIFICATIONS WILL GO AWAY! THIS IS ALSO SERIOUS
  //  //Meteor.users.remove({}); // EVEN THE USERS! THINK OF THE CHILDREN!!
  //  Ratings.remove({}); // well these weren't really working anyways... ¯\_(ツ)_/¯
   //
  //  var numUsers = 20;
  //  var numDrivers = 10;
   //
  //  var users = [];
  //  // users[0...numDrivers-1] are drivers
   //
   //
   //
  //  //create users
  //  for(var i = 0; i < numUsers; i++) {
  //    var fname = faker.name.firstName();
  //    var lname = faker.name.lastName();
  //    var fullname = fname + " " + lname;
  //    var em = faker.internet.email();
  //    var prof = {
  //      // facebookID: 000000000000001,  //for testing, but doesn't really do anything helpful / does this file even do anything?
  //      address : "123 Main St., Bozeman, MT 59715",
  //      birthDate : faker.date.past(),
  //      age: 25,
  //      phone: "4061234567",
  //      skierOrBoarder: "skier",
  //      vehicle : {
  //          make : "Subaru",
  //          model : "Outback",
  //          year : 2001,
  //          seats : 4,
  //          color : "blue",
  //          MPG : 30
  //        },
  //         settings : {
  //          defaultDestination : 1,
  //          defaultPickupLocation: null,
  //          defaultDepartureTime : null,
  //          preferredAgeRangeMin : 18,
  //          preferredAgeRangeMax : 25,
  //          defaultCity : "Bozeman"
   //
  //        },
  //        numRequestsSent : 0,
  //    		hasOnboarded : false,
  //    		currentLocation : null
  //    };
  //    if(i < numDrivers) {
  //      prof.vehicle = {
  //          make : "Subaru", //constant, everything will break if changed
  //          model : "Forester",
  //          year : 1964,
  //          seats : 2,
  //          color : "blue",
  //          MPG : 30
  //      };
  //    };
   //
  //    user = Accounts.createUser({email : em, password : "sharelift", profile : prof});
   //
  //    //fake the data that will be pulled from fb
  //    Meteor.users.update(
  //      {_id : user},
  //      {$set : {
  //        "services.facebook" : {
  //          email : em,
  //          first_name : fname,
  //          last_name : lname,
  //          name : fullname,
  //          gender : "male", //idk how to do random genders with faker.js; yolo
  //          locale : "en_US"
  //        }
  //      }});
   //
  //    user = Meteor.users.findOne({_id : user}); //get actual user object
  //    if(i < numDrivers) {
  //      Roles.addUsersToRoles(user._id, ["driver"]); //
  //    };
  //    users.push(user);
  //    // console.log("user " + user.services.facebook.name + " created / " + user._id);
  //  }
   //
  //  //baller status. now put in some open lifts
  //  //technically violates the constraint of not being able to be potential rider with open lift offer but yolo
  //  for(var i = 0; i < numDrivers; i++) {
  //    var randLiftDest = Math.floor(Math.random()*(2-1+1)+1);
  //    var randDay = Math.floor(Math.random()*(2-0+1)+0);
  //    var randHour = Math.floor(Math.random()*(10-2+1)+2);
   //
  //    var departureDay = moment().add(randDay, 'days');
  //    var fakeDepartureTime = moment(departureDay).add(randHour, 'hours');
  //    var returnTime = moment(fakeDepartureTime).add(4, 'hours');
  //    var creationTime = moment(fakeDepartureTime).subtract(2, 'hours').toDate();
  //    var direction;
  //    if(i < 4){  //make the first 5 lifts toDestination, and the last 5 fromDestination
  //      direction = 'toDestination';
  //    }
  //    else{
  //      direction = 'fromDestination';
  //    }
   //
  //    var fakePickupDropoffLocation = {
  //      fullAddress: "1234 S. Example St. Bozeman, MT 59715",
  //      vicinity: "Bozeman",
  //      lat: 45.6794836,
  //      long: -111.0352398
  //    };
  //    var fakeDriverHomeLocation = {
  //      fullAddress: "1234 S. Example St. Bozeman, MT 59715",
  //      vicinity: "Bozeman",
  //      lat: 45.6794900,
  //      long: -111.0352900
  //    };
  //    var fakeDestinationLocation = {
  //      fullAddress: "50 Big Sky Resort Road, Big Sky, MT 59716",
  //      vicinity: "Big Sky",
  //      lat: 45.2865,
  //      long: -111.3999
  //    }
   //
  //    Lifts.insert({
  //      isFake : true,
  //      driverId: users[i]._id,
  //      inputTime: creationTime,
  //      driverName: users[i].services.facebook.first_name + " " + users[i].services.facebook.last_name.charAt(0),
  //      seats: 4,
  //      driverPhone:"4061234567",
  //      date: departureDay.toDate(),
  //      potentialRiders: [{
  //        userId: users[i+1]._id,
  //        userName: users[i+1].services.facebook.first_name +  " " + users[i+1].services.facebook.last_name.charAt(0),
  //        pickupDropoffLocation: fakePickupDropoffLocation,
  //        pickupTime: moment(fakeDepartureTime).add(20, 'minutes').toDate(),
  //        phone:"4061234567",
  //      },
  //      {
  //        userId: users[i+2]._id,
  //        userName: users[i+2].services.facebook.first_name +  " " + users[i+2].services.facebook.last_name.charAt(0),
  //        pickupDropoffLocation: fakePickupDropoffLocation,
  //        pickupTime: moment(fakeDepartureTime).add(20, 'minutes').toDate(),
  //        phone:"4061234567",
  //      }],
  //      confirmedRiders: [
  //      {
  //        userId: users[i+3]._id,
  //        userName: users[i+3].services.facebook.first_name +  " " + users[i+3].services.facebook.last_name.charAt(0),
  //        pickupDropoffLocation: fakePickupDropoffLocation,
  //        pickupTime: moment(fakeDepartureTime).add(20, 'minutes').toDate(),
  //        phone:"4061234567",
  //      },
  //      {
  //        userId: users[i+4]._id,
  //        userName: users[i+4].services.facebook.first_name +  " " + users[i+4].services.facebook.last_name.charAt(0),
  //        pickupDropoffLocation: fakePickupDropoffLocation,
  //        pickupTime: moment(fakeDepartureTime).add(20, 'minutes').toDate(),
  //        phone:"4061234567",
  //      }
  //      ],
  //      currentRiders: [],
  //      deniedRiders: [],
  //      liftDirection : 'toDestination',
  //      destinationLocationId : randLiftDest,
  //      startEndLocation : fakeDriverHomeLocation, // a location object. This would be the location of the destination if this lift had liftDirection = fromDestination
  //      endLocation : fakeDestinationLocation,
   //
  //      startSector : null,
  //      estimatedTravelTime: 60, // estimated time in minutes
  //      distanceTraveled: 0,  //starts at 0, but will need to be updated upon lift completion.
  //      departureTime : moment(fakeDepartureTime).toDate(),
  //      returnTime : '',
  //      oppositeDirectionLiftId : '',
   //
  //      isActive : false,
  //      driverEnRoute : false,
  //      hasArrivedAtDestination: false, //true when the vehicle arrives at the destination (only for toDestination lifts)
  //      isComplete : false,
   //
  //      estimatedCost : 0,
  //      fees : 0,
  //      payments : [],
  //      driverPayment : 0
  //    });
  //  }

});

if(Meteor.isServer){
  Meteor.methods({

    // getFBUser : function() {
    //   var fb = new Facebook(Meteor.user().services.facebook.accessToken);
    //   return fb;
    //
    // }
    getFBUserData: function() {//returns json object w/ FB user data
        var fb = new Facebook(Meteor.user().services.facebook.accessToken);
        var data = fb.getFBUserData();
        console.log("getUserData was called!");
        return data;

    },
    pullFBFriendsData: function() {//returns a json object w/ list of FB friends who have signed into the app before
    var fb = new Facebook(Meteor.user().services.facebook.accessToken);
    var friendList = fb.getFBFriendsData();
    //add friends to database
    console.log("PullFBFriends() called successfully:");
    console.log(friendList);

    var friendIDs = [];
        //=============================================================================================
    //All of this is to deal with paging in the facebook graph results:
    var lastPage = false;
    while(!lastPage){
      // console.log("The length of data is: " + friendList.data.length);
      for(var i = 0; i < friendList.data.length; i++){
        friendIDs.push(friendList.data[i].id);
        // console.log("Added user with id of " + friendList.data[i].id);
      }
      if(friendList.paging.hasOwnProperty("next")){  //repeat the while loop only if there is a 'next' attribute in 'paging' in the list returned from Facebook
        // console.log("Has property next. Should call api again.");
        //parse the paging next address:
        var startIndex = friendList.paging.next.search("friends");
        var shortAddress = "/me/" + friendList.paging.next.slice(startIndex);
        // console.log("The shortAddress is " + shortAddress);

        friendList = fb.customeFacebookGraphCall(shortAddress);
      }
      else{
        lastPage = true;
      }
    }
    //=============================================================================================

    //then add it to the database
    Meteor.users.update(Meteor.userId(), {$set : {'profile.facebookFriends' : friendIDs} });  //add the array of Facebook friends to user profile

  },


  pullFBPicture: function() { //adds the url of the user's facebook profile picture to user.profile.profilePicture
      //var fb = new Facebook(Meteor.user().services.facebook.accessToken);
      //var picData = fb.getFBPicture();
      //var picUrl = picData.picture.data.url;

      //If the user already has a profile photo, do nothing.
      if(Meteor.user().profile.profilePicture){
        return;
      }
      //If the user does not have a profile photo, pull in their facebook profile pic
      else{

        // <<< This is the logic for pulling in FB profile picture.

        let id = Meteor.user().services.facebook.id;
        let picUrl = "http://graph.facebook.com/" + id + "/picture/?type=large"
        Meteor.users.update(Meteor.userId(), {$set : {'profile.profilePicture' : picUrl} });  //add url to profilePicture user attribute
        // console.log("Your FB profile photo is: " + Meteor.user().profile.profilePicture);

        //This is for setting the profile pic to profile_filler2.png, rather than pulling the url from FB.
        // Meteor.users.update(Meteor.userId(), {$set : {'profile.profilePicture' : "profile_filler2.png"} });


      }



  },

  //sets the profile variable hasConnectedFacebook to true
  setHasConnectedFacebook: function() {
    Meteor.users.update(Meteor.userId(), {$set : {'profile.hasConnectedFacebook' : true} });
  },

//Thought I needed to copy each user's FB ID from services to profile, but ya dont. "Spend an abominable amount of time being confused and learn" I guess...
  // setFacebookIdInProfile: function() {//called upon FB login. Copies services.facebook.id to profile.facebookID
  //   var fBiD = Meteor.user().services.facebook.id;
  //   console.log("Called setFacebookIdInProfile. This is your fb ID: " + fBiD);
  //   Meteor.users.update(Meteor.userId(), {$set : {'profile.facebookID' : fBiD} });
  // }



//MORE FACEBOOK TO-DOs:

  //Find Facebook friends who are already using the app:
  // 1) Store the FB ID for the logged in user 2) call user/friends to get a list of friends that have logged into the app before

//Notify user when their facebook friends sign up:
//1) When a new user logs in, use me/friends, and then identify the FB ids that were returned (and represent friends who are ShareLift users). Send notifications to these ids

//Send notifications to FB friends when you post a lift:
//If the feature is turned on, 1) call user/friends to get the list of FB ids for friends who are ShareLift users. 2) Go through and send notifications to FB users who a) haven't already posted a lift and b) have the notification feature turned on





  });

  function Facebook(accessToken) {
    this.fb = Meteor.npmRequire('fbgraph'); //loads the fbgraph module specified in packages.json
    this.accessToken = accessToken;
    this.fb.setAccessToken(this.accessToken);
    this.options = {
        timeout: 3000,
        pool: {maxSockets: Infinity},
        headers: {connection: "keep-alive"}
    }
    this.fb.setOptions(this.options);
  }

//Not sure if this is just  templte or not. I don't think so. I think you can change it to either 'get' or 'post'
  Facebook.prototype.query = function(query, method) {
      var self = this;
      var method = (typeof method === 'undefined') ? 'get' : method;
      var data = Async.runSync(function(done) {
          self.fb[method](query, function(err, res) {
              done(null, res);
          });
      });
      return data.result;
  }

Facebook.prototype.getFBUserData = function() {
    return this.query('me');
}

Facebook.prototype.getFBFriendsData = function() {
    return this.query('/me/friends');
}

Facebook.prototype.getFBPicture = function() {
  return this.query('me?fields=picture.height(2000)');
}

//Custom calls to Facebook graph api -- Must input properly formated string address (Ex: '/me/friends')
Facebook.prototype.customeFacebookGraphCall = function(address) {
    return this.query(address);
}

}

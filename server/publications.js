Meteor.publish('getPendingLiftRequests', function(args) {
	//return Lifts.find({"potentialRiders.userId" : args.fbID});
	// ---------------  Need to specify which fields to include ----------- //
	return Lifts.find({potentialRiders : {$elemMatch: {userId: this.userId}}});
});

Meteor.publish('getOpenLifts', function(args) {
	// ---------------  Need to specify which fields to include (it should be only the info non-riders/drivers are allowed to see)----------- //
    return Lifts.find({isComplete : false});
});

Meteor.publish('getOpenRequests', function(args) {
	// ---------------  Need to specify which fields to include (it should be only the info non-riders/drivers are allowed to see)----------- //
    return RideRequests.find({isAccepted : false});
});

// I'm guessing I'll want to get rid of this
Meteor.publish('getOneLift', function(id) {
    return Lifts.find({_id : id});
});

//publishes lift data specific to driver
Meteor.publish('liftsWhereDriver', function() {
	// ---------------  Need to specify which fields to include ----------- //
    return Lifts.find({isComplete : false, driverId : this.userId});
});

//publishes lift data specific to rider
Meteor.publish('liftsWhereRider', function() {
	// ---------------  Need to specify which fields to include ----------- //

	return Lifts.find({confirmedRiders : {$elemMatch: {userId: this.userId}}});
});



Meteor.publish('getPastLiftsDriver', function(args) {
    return Lifts.find({isComplete : true, driverId : this.userId});
});

Meteor.publish('getPastLiftsRider', function(args) {
	//return Lifts.find({"potentialRiders.userId" : args.fbID});
	//it seems like I probably also want to add the constraint isComplete : true ?
	return Lifts.find({confirmedRiders : {$elemMatch: {userId: this.userId}}});
});

//=========================

Meteor.publish('getLocations', function(args) {
	return Locations.find({});
});

//=========================

Meteor.publish("getUserData", function () {
	return Meteor.users.find({_id: this.userId});
});

Meteor.publish("getPublicProfileData", function() {
	var usr = Meteor.users.find({},
		{
		'profile.profilePicture' : 1,	//right now we're only publishing profile photos
		'profile.vehicle' : 1,	//returns all Users's vehicle photos. Probably not very secure (should be it's own publication)
		'profile.services.facebook.name' : 1,
		'profile.services.facebook.first_name' : 1,
		'profile.settings.defaultCity' : 1,
		'profile.settings.defaultDestination' : 1,
		'profile.bio' : 1,
		'profile.music' : 1,
		'profile.preferredAgeRangeMin' : 1,
		'profile.preferredAgeRangeMax' : 1,
		'services.facebook.gender' : 1,
		'profile.facebookFriends' : 1
			
		}
	);
	return usr;
});

//Publishes publically accessible data for all users. Limit the amount of data you publish here
Meteor.publish("allUserData", function () {
	return Meteor.users.find(
		{},	//all users
		{fields: {
			'profile.profilePicture' : 1,	//right now we're only publishing profile photos
			'profile.vehicle' : 1,	//returns all Users's vehicle photos. Probably not very secure (should be it's own publication)
			'profile.services.facebook.name' : 1,
			'profile.services.facebook.first_name' : 1
		}
	});
});



//=========================

Meteor.publish("getRiderNotificationsConfirm", function () {
	return Notifications.find({riderId : this.userId, notifType : "confirmLift"});
});

Meteor.publish("getRiderNotificationsDeny", function () {
	return Notifications.find({riderId : this.userId, notifType : "denyLift"});
});

Meteor.publish("getDriverNotifications", function () {
	return Notifications.find({driverId : this.userId, notifType : "requestLift"});
});

Meteor.publish("getCancelDriverNotifications", function () {
	return Notifications.find({driverId : this.userId, notifType : "canceledRider"});
});

Meteor.publish("getCancelRiderNotifications", function () {
	return Notifications.find({riderId : this.userId, notifType : "cancelConfirmation"});
});

Meteor.publish("getDeletedLiftNotifications", function () {
	return Notifications.find({riderId : this.userId, notifType : "deletedLift"});
});

//Not sure why we had seperate publications for each type of notification...
// this one combines all the rider-side notifications
Meteor.publish("getAllRiderNotifications", function () {
	return Notifications.find(
		{riderId : this.userId,
			$or: [
				{notifType : "confirmLift"},
				{notifType : "denyLift"},
				{notifType : "cancelConfirmation"},
				{notifType : "deletedLift"},
				{notifType : "driverIsHere"},
				{notifType : "driverOnTheWay"},
				{notifType : "sendOffer"},
				{notifType : "confirmOffer"},
			]
		});
});
//=========================

Meteor.publish("getLiftRatings", function (args) {
	console.log(args);
	return Ratings.find({liftId : args});
});

Meteor.publish("getRatings", function(){
	return Ratings.find({});
});

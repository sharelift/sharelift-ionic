Meteor.methods({


  // MOST OF THESE METHODS ARE OUT OF DATE - THEY DON't REFLECT CURRENT DATA STRUCTURES

  // resetData: function(){ // THIS IS A SUPER DUPER SCARY FUNCTION!!!! ONLY FREAKING CALL IT IF YOU'RE SUPER SURE YOU NEED IT.
  //   Lifts.remove({}); // ALL LIFTS WILL GO AWAY! THIS IS SERIOUS
  //   Notifications.remove({}); // ALL THE NOTIFICATIONS WILL GO AWAY! THIS IS ALSO SERIOUS
  //   Meteor.users.remove({}); // EVEN THE USERS! THINK OF THE CHILDREN!!
  //   Ratings.remove({}); // well these weren't really working anyways... ¯\_(ツ)_/¯
  // },
  //
  // insertFixtureLifts: function(){
  //   var numUsers = 20;
  //   var numDrivers = 10;
  //
  //   var users = [];
  //   // users[0...numDrivers-1] are drivers
  //
  //
  //
  //   //create users
  //   for(var i = 0; i < numUsers; i++) {
  //     var fname = faker.name.firstName();
  //     var lname = faker.name.lastName();
  //     var fullname = fname + " " + lname;
  //     var em = faker.internet.email();
  //     var prof = {
  //       // facebookID: 000000000000001,  //for testing, but doesn't really do anything helpful / does this file even do anything?
  //       address : "123 Main St., Bozeman, MT 59715",
  //       birthDate : faker.date.past(),
  //       age : null,
  //       phone: "4061234567",
  //       skierOrBoarder : "",
  //       vehicle : {
  //           make : "Subaru",
  //           model : "Outback",
  //           year : 2001,
  //           seats : 4,
  //           color : "Green",
  //           MPG : 35
  //         },
  //       settings : {
  //           defaultDestination : 1,
  //           defaultPickupLocation : "123 Main St., Bozeman, MT 59715",
  //           defaultDepartureTime : null,
  //           preferredAgeRangeMin : 18,
  //           preferredAgeRangeMax : 25,
  //           defaultCity : ""
  //         },
  //       numRequestsSent : 0,
  //       hasOnboarded : true,
  //       currentLift : {},
  //     };
  //     if(i < numDrivers) {
  //       prof.vehicle = {
  //           make : "Subaru", //constant, everything will break if changed
  //           model : "Forester",
  //           year : 1964,
  //           seats : 2
  //       };
  //     };
  //
  //     user = Accounts.createUser({email : em, password : "sharelift", profile : prof});
  //
  //     //fake the data that will be pulled from fb
  //     Meteor.users.update(
  //       {_id : user},
  //       {$set : {
  //         "services.facebook" : {
  //           email : em,
  //           first_name : fname,
  //           last_name : lname,
  //           name : fullname,
  //           gender : "male", //idk how to do random genders with faker.js; yolo
  //           locale : "en_US"
  //         }
  //       }});
  //
  //     user = Meteor.users.findOne({_id : user}); //get actual user object
  //     if(i < numDrivers) {
  //       Roles.addUsersToRoles(user._id, ["driver"]); //
  //     };
  //     users.push(user);
  //     // console.log("user " + user.services.facebook.name + " created / " + user._id);
  //   }
  //
  //   //baller status. now put in some open lifts
  //   //technically violates the constraint of not being able to be potential rider with open lift offer but yolo
  //   for(var i = 0; i < numDrivers; i++) {
  //     var randLiftDest = Math.floor(Math.random()*(2-1+1)+1);
  //     var randDay = Math.floor(Math.random()*(2-0+1)+0);
  //     var randHour = Math.floor(Math.random()*(10-2+1)+2);
  //
  //     var departureDay = moment().add(randDay, 'days');
  //     var departureTime = moment(departureDay).add(randHour, 'hours');
  //     var returnTime = moment(departureTime).add(4, 'hours');
  //
  //     Lifts.insert({
  //       driverId: users[i]._id,
  //       driverName: users[i].services.facebook.first_name + " " + users[i].services.facebook.last_name.charAt(0),
  //       seats: 4,
  //       driverPhone:"4061234567",
  //       date: departureDay.toDate(),
  //       potentialRiders: [{
  //         userId: users[i+1]._id,
  //         userName: users[i+1].services.facebook.first_name +  " " + users[i+1].services.facebook.last_name.charAt(0),
  //         pickupDropoffLocation: users[i+1].profile.address,
  //         pickupTime: "9:15 a.m."
  //       },
  //       {
  //         userId: users[i+2]._id,
  //         userName: users[i+2].services.facebook.first_name +  " " + users[i+2].services.facebook.last_name.charAt(0),
  //         pickupDropoffLocation: users[i+2].profile.address,
  //         pickupTime: "9:15 a.m."
  //       }],
  //       confirmedRiders: [
  //       {
  //         userId: users[i+3]._id,
  //         userName: users[i+3].services.facebook.first_name +  " " + users[i+3].services.facebook.last_name.charAt(0),
  //         pickupDropoffLocation: users[i+3].profile.address,
  //         pickupTime: "9:15 a.m."
  //       },
  //       {
  //         userId: users[i+4]._id,
  //         userName: users[i+4].services.facebook.first_name +  " " + users[i+4].services.facebook.last_name.charAt(0),
  //         pickupDropoffLocation: users[i+4].profile.address,
  //         pickupTime: "9:15 a.m."
  //       }
  //       ],
  //       currentRiders: [],
  //       liftDirection: 'toDestination',
  //       roundTrip: true,
  //       destinationLocationId: randLiftDest,
  //       startEndLocation: '308 S. 13th Ave., Bozeman, MT 59715',
  //       departureTime: departureTime.toDate(),
  //       returnTime : returnTime.toDate(),
  //       oppositeDirectionLiftId: '',
  //       isActive: false,
  //       driverEnRoute: false,
  //       isComplete: false
  //     });
  //   }
  // },
  // insertFixtureNotifications: function() {
  //   let lift1 = Lifts.findOne({});
  //   let lift2 = Lifts.findOne({_id:{$ne:lift1._id}});
  //   let lift3  = Lifts.findOne({id:{$nin:[lift1._id, lift2._id]}});
  //
  //   console.log(lift1);
  //   console.log(lift2);
  //   console.log(lift3);
  //   //Lift 1
  //   Notifications.insert({
  //     driverId: lift1.driverId,
  //     riderId: Meteor.userId(),
  //     liftId: lift1._id,
  //     userName : lift1.driverName,
  //     read: false,
  //     notifType: "confirmLift",
  //     date: new Date(),
  //     dismissed:false
  //   });
  //
  //   Notifications.insert({
  //     driverId: lift2.driverId,
  //     riderId: Meteor.userId(),
  //     liftId: lift2._id,
  //     userName : lift2.driverName,
  //     read: false,
  //     notifType: "denyLift",
  //     date: new Date(),
  //     dismissed:false
  //   });
  //
  //   Notifications.insert({
  //     driverId: Meteor.userId(),
  //     riderId: lift3.driverId,
  //     liftId: lift3._id,
  //     userName : lift3.driverName,
  //     read: false,
  //     notifType: "requestLift",
  //     date: new Date(),
  //     dismissed:false
  //   });
  // },
  // insertFixtureOpenLiftDriver: function() {
  //   var user1 = Meteor.call("createFixturesUser");
  //   var user2 = Meteor.call("createFixturesUser");
  //   var rTime = moment(new Date()).add(3, 'minutes').toDate();
  //
  //   var lift = {
  //     driverId: Meteor.userId(),
  //     inputTime: new Date(),
  //     driverName: Meteor.user().services.facebook.first_name + " " + Meteor.user().services.facebook.last_name.charAt(0) + ".",
  //     seats: 4, //
  //     date: new Date(),
  //     potentialRiders: [{
  //       userId: user1._id,
  //       userName: user1.services.facebook.first_name + " " + user1.services.facebook.last_name.charAt(0) + ".",
  //       pickupDropoffLocation: user1.profile.address,
  //       pickupTime: "9:15 a.m."
  //     }, {
  //       userId: user2._id,
  //       userName: user2.services.facebook.first_name + " " + user2.services.facebook.last_name.charAt(0) + ".",
  //       pickupDropoffLocation: user2.profile.address,
  //       pickupTime: "9:15 a.m."
  //     }],
  //     confirmedRiders: [],
  //     deniedRiders: [],
  //     currentRiders: [],
  //     liftDirection: 'toDestination',
  //     roundTrip: true,
  //     destinationLocationId: 1,
  //     startEndLocation: "SUB",
  //     departureTime: new Date(),
  //     returnTime: rTime,
  //     oppositeDirectionLiftId: '',
  //     isActive: true,
  //     driverEnRoute: false,
  //     isComplete: false
  //   }
  //   lId = Lifts.insert(lift);
  //
  //   Meteor.users.update(Meteor.userId(), {
  //     $set: {
  //       "profile.currentLift.liftId": lId,
  //       "profile.currentLift.departureTime": new Date(),
  //       "profile.currentLift.returnTime": rTime,
  //       "profile.currentLift.liftRole": "driver",
  //       "profile.currentLift.ratingNeeded": false,
  //     }
  //   });
  // },
  // insertFixtureOpenLiftRider: function() {
  //   var user1 = Meteor.call("createFixturesUser");
  //   var user2 = Meteor.call("createFixturesUser");
  //   var rTime = moment(new Date()).add(3, 'minutes').toDate();
  //
  //   var lift = {
  //     driverId: user1._id,
  //     inputTime: new Date(),
  //     driverPhone:"4061234567",
  //     driverName: user1.services.facebook.first_name + " " + user1.services.facebook.last_name.charAt(0) + ".",
  //     seats: 4,
  //     date: new Date(),
  //     potentialRiders: [],
  //     confirmedRiders: [{
  //       userId: Meteor.userId(),
  //       userName: Meteor.user().services.facebook.first_name + " " + Meteor.user().services.facebook.last_name.charAt(0) + ".",
  //       pickupDropoffLocation: "3423 Main St.",
  //       pickupTime: "9:15 a.m."
  //     }, {
  //       userId: user2._id,
  //       userName: user2.services.facebook.first_name + " " + user2.services.facebook.last_name.charAt(0) + ".",
  //       pickupDropoffLocation: user2.profile.address,
  //       pickupTime: "9:15 a.m."
  //     }],
  //     deniedRiders: [],
  //     currentRiders: [],
  //     liftDirection: 'toDestination',
  //     roundTrip: true,
  //     destinationLocationId: 1,
  //     startEndLocation: "SUB",
  //     departureTime: new Date(),
  //     returnTime: rTime,
  //     oppositeDirectionLiftId: '',
  //     isActive: true,
  //     driverEnRoute: false,
  //     isComplete: false
  //   } //
  //   lId = Lifts.insert(lift);
  //
  //   Meteor.users.update(Meteor.userId(), {
  //     $set: {
  //       "profile.currentLift.liftId": lId,
  //       "profile.currentLift.departureTime": new Date(),
  //       "profile.currentLift.returnTime": rTime,
  //       "profile.currentLift.liftRole": "rider",
  //       "profile.currentLift.ratingNeeded": false,
  //     }
  //   });
  //
  //   var now = new Date();
  //   Notifications.insert({
  //     driverId: user1._id,
  //     riderId: Meteor.userId(),
  //     userName :  user1.services.facebook.first_name + " " + user1.services.facebook.last_name.charAt(0) + ".",
  //     liftId: lId,
  //     read: false,
  //     notifType: "confirmLift",
  //     date: now,
  //     dismissed:false
  //   });
  // },
  //
  // insertFixtureOpenLiftDriverFarOffDude: function() {
  //   var user1 = Meteor.call("createFixturesUser");
  //   var user2 = Meteor.call("createFixturesUser");
  //   var dTime = moment(new Date()).add(4000, 'minutes').toDate();
  //   var rTime = moment(new Date()).add(4400, 'minutes').toDate();
  //
  //   var lift = {
  //     driverId: Meteor.userId(),
  //     inputTime: new Date(),
  //     driverName: Meteor.user().services.facebook.first_name + " " + Meteor.user().services.facebook.last_name.charAt(0) + ".",
  //     seats: 4, //
  //     date: new Date(),
  //     potentialRiders: [{
  //       userId: user1._id,
  //       userName: user1.services.facebook.first_name + " " + user1.services.facebook.last_name.charAt(0) + ".",
  //       pickupDropoffLocation: "123 West Main Street",
  //       pickupTime: "9:15 a.m.",
  //       user:{profile: {phone:"4061234567"}}
  //     }, {
  //       userId: user2._id,
  //       userName: user2.services.facebook.first_name + " " + user2.services.facebook.last_name.charAt(0) + ".",
  //       pickupDropoffLocation: "123 East Main Street",
  //       pickupTime: "9:15 a.m.",
  //       user: {profile: {phone:"4068910123"}}
  //     }],
  //     confirmedRiders: [],
  //     deniedRiders: [],
  //     currentRiders: [],
  //     liftDirection: 'toDestination',
  //     roundTrip: true,
  //     destinationLocationId: 1,
  //     startEndLocation: "SUB",
  //     departureTime: dTime,
  //     returnTime: rTime,
  //     oppositeDirectionLiftId: '',
  //     isActive: true,
  //     driverEnRoute: false,
  //     isComplete: false
  //   }
  //   lId = Lifts.insert(lift);
  //
  //   Notifications.insert({
  //     driverId: Meteor.userId(),
  //     riderId: user1._id,
  //     liftId: lId,
  //     userName :  user1.services.facebook.first_name + " " + user1.services.facebook.last_name.charAt(0) + ".",
  //     read: false,
  //     notifType: "requestLift",
  //     date: new Date(),
  //     dismissed:false
  //   })
  //   Notifications.insert({
  //     driverId: Meteor.userId(),
  //     riderId: user2._id,
  //     liftId: lId,
  //     userName :  user2.services.facebook.first_name + " " + user2.services.facebook.last_name.charAt(0) + ".",
  //     read: false,
  //     notifType: "requestLift",
  //     date: new Date(),
  //     dismissed:false
  //   })
  //   Meteor.users.update(Meteor.userId(), {
  //     $set: {
  //       "profile.currentLift.liftId": lId,
  //       "profile.currentLift.departureTime": dTime,
  //       "profile.currentLift.returnTime": rTime,
  //       "profile.currentLift.liftRole": "driver",
  //       "profile.currentLift.ratingNeeded": false,
  //     }
  //   });
  // },
  // insertFixtureOpenLiftRiderFarOffDude: function() {
  //   var user1 = Meteor.call("createFixturesUser");
  //   var user2 = Meteor.call("createFixturesUser");
  //   var dTime = moment(new Date()).add(4000, 'minutes').toDate();
  //   var rTime = moment(new Date()).add(4400, 'minutes').toDate();
  //
  //   var lift = {
  //     driverId: user1._id,
  //     inputTime: new Date(),
  //     driverName: user1.services.facebook.first_name + " " + user1.services.facebook.last_name.charAt(0) + ".",
  //     seats: 4,
  //     date: new Date(),
  //     driverPhone:"4061234567",
  //     potentialRiders: [],
  //     confirmedRiders: [{
  //       userId: Meteor.userId(),
  //       userName: Meteor.user().services.facebook.first_name + " " + Meteor.user().services.facebook.last_name.charAt(0) + ".",
  //       pickupDropoffLocation: "3423 Main St.",
  //       pickupTime: "9:15 a.m."
  //     }, {
  //       userId: user2._id,
  //       userName: user2.services.facebook.first_name + " " + user2.services.facebook.last_name.charAt(0) + ".",
  //       pickupDropoffLocation: user2.profile.address,
  //       pickupTime: "9:15 a.m."
  //     }],
  //     deniedRiders: [],
  //     currentRiders: [],
  //     liftDirection: 'toDestination',
  //     roundTrip: true,
  //     destinationLocationId: 1,
  //     startEndLocation: "SUB",
  //     departureTime: dTime,
  //     returnTime: rTime,
  //     oppositeDirectionLiftId: '',
  //     isActive: true,
  //     driverEnRoute: false,
  //     isComplete: false
  //   } //
  //   lId = Lifts.insert(lift);
  //
  //   Meteor.users.update(Meteor.userId(), {
  //     $set: {
  //       "profile.currentLift.liftId": lId,
  //       "profile.currentLift.departureTime": dTime,
  //       "profile.currentLift.returnTime": rTime,
  //       "profile.currentLift.liftRole": "rider",
  //       "profile.currentLift.ratingNeeded": false,
  //     }
  //   });
  //
  //   var now = new Date();
  //   Notifications.insert({
  //     driverId: user1._id,
  //     riderId: Meteor.userId(),
  //     userName :user1.services.facebook.first_name + " " + user1.services.facebook.last_name.charAt(0) + ".",
  //     liftId: lId,
  //     read: false,
  //     notifType: "confirmLift",
  //     date: now,
  //     dismissed:false
  //   });
  // },
  //
  //
  // insertFixtureCompletedLiftDriver: function() {
  //   var user1 = Meteor.call("createFixturesUser");
  //   var user2 = Meteor.call("createFixturesUser");
  //   var rTime = moment(new Date()).subtract(200, 'minutes').toDate();
  //   var dTime = moment(new Date()).subtract(600, 'minutes').toDate();
  //
  //   var lift = {
  //     driverId: Meteor.userId(),
  //     inputTime: new Date(),
  //     driverName: Meteor.user().services.facebook.first_name + " " + Meteor.user().services.facebook.last_name.charAt(0) + ".",
  //     seats: 4,
  //     date: new Date(),
  //     potentialRiders: [],
  //     confirmedRiders: [{
  //       userId: user1._id,
  //       userName: user1.services.facebook.first_name + " " + user1.services.facebook.last_name.charAt(0) + ".",
  //       pickupDropoffLocation: user1.profile.address,
  //       pickupTime: "9:15 a.m."
  //     }, {
  //       userId: user2._id,
  //       userName: user2.services.facebook.first_name + " " + user2.services.facebook.last_name.charAt(0) + ".",
  //       pickupDropoffLocation: user2.profile.address,
  //       pickupTime: "9:15 a.m."
  //     }],
  //     deniedRiders: [],
  //     currentRiders: [],
  //     liftDirection: 'toDestination',
  //     roundTrip: true,
  //     destinationLocationId: 1,
  //     startEndLocation: "SUB",
  //     departureTime: dTime,
  //     returnTime: rTime,
  //     oppositeDirectionLiftId: '',
  //     isActive: true,
  //     driverEnRoute: false,
  //     isComplete: true
  //   } //
  //   lId = Lifts.insert(lift);
  //
  // },
  // insertFixtureCompletedLiftDriverRequireRatings: function() {
  //   var user1 = Meteor.call("createFixturesUser");
  //   var user2 = Meteor.call("createFixturesUser");
  //   var rTime = moment(new Date()).subtract(200, 'minutes').toDate();
  //   var dTime = moment(new Date()).subtract(600, 'minutes').toDate();
  //
  //   var lift = {
  //     driverId: Meteor.userId(),
  //     inputTime: new Date(),
  //     driverName: Meteor.user().services.facebook.first_name + " " + Meteor.user().services.facebook.last_name.charAt(0) + ".",
  //     seats: 4,
  //     date: new Date(),
  //     potentialRiders: [],
  //     confirmedRiders: [{
  //       userId: user1._id,
  //       userName: user1.services.facebook.first_name + " " + user1.services.facebook.last_name.charAt(0) + ".",
  //       pickupDropoffLocation: user1.profile.address,
  //       pickupTime: "9:15 a.m."
  //     }, {
  //       userId: user2._id,
  //       userName: user2.services.facebook.first_name + " " + user2.services.facebook.last_name.charAt(0) + ".",
  //       pickupDropoffLocation: user2.profile.address,
  //       pickupTime: "9:15 a.m."
  //     }],
  //     deniedRiders: [],
  //     currentRiders: [],
  //     liftDirection: 'toDestination',
  //     roundTrip: true,
  //     destinationLocationId: 1,
  //     startEndLocation: "SUB",
  //     departureTime: dTime,
  //     returnTime: rTime,
  //     oppositeDirectionLiftId: '',
  //     isActive: true,
  //     driverEnRoute: false,
  //     isComplete: true
  //   } //
  //   lId = Lifts.insert(lift);
  //
  //   Meteor.users.update(Meteor.userId(), {
  //     $set: {
  //       "profile.currentLift.liftId": null,
  //       "profile.currentLift.departureTime": null,
  //       "profile.currentLift.returnTime": null,
  //       "profile.currentLift.liftRole": null,
  //       "profile.currentLift.ratingNeeded": true,
  //     }
  //   });
  // },
  //
  // insertFixtureDriverRatings: function(){
  //   let rating = {};
  //   rating.ratingType = 'driverRating';
  //   rating.liftId = "efg5678";
  //   rating.riderId = "abcd1234";
  //   rating.driverId = Meteor.userId();
  //   rating.rating1 = parseInt(3);
  //   rating.rating2 = parseInt(3);
  //   rating.rating3 = parseInt(3);
  //   Meteor.call('submitRating', rating);
  // },
  // insertFixtureCompletedLiftRider: function() {
  //   var user1 = Meteor.call("createFixturesUser");
  //   var user2 = Meteor.call("createFixturesUser");
  //   var rTime = moment(new Date()).subtract(200, 'minutes').toDate();
  //   var dTime = moment(new Date()).subtract(600, 'minutes').toDate();
  //
  //   var lift = {
  //     driverId: user1._id,
  //     inputTime: new Date(),
  //     driverName: user1.services.facebook.first_name + " " + user1.services.facebook.last_name.charAt(0) + ".",
  //     seats: 4,
  //     date: new Date(),
  //     potentialRiders: [],
  //     confirmedRiders: [{
  //       userId: Meteor.userId(),
  //       userName: Meteor.user().services.facebook.first_name + " " + Meteor.user().services.facebook.last_name.charAt(0) + ".",
  //       pickupDropoffLocation: "3423 Main St.",
  //       pickupTime: "9:15 a.m."
  //     }, {
  //       userId: user2._id,
  //       userName: user2.services.facebook.first_name + " " + user2.services.facebook.last_name.charAt(0) + ".",
  //       pickupDropoffLocation: user2.profile.address,
  //       pickupTime: "9:15 a.m."
  //     }],
  //     deniedRiders: [],
  //     currentRiders: [],
  //     liftDirection: 'toDestination',
  //     roundTrip: true,
  //     destinationLocationId: 1,
  //     startEndLocation: "SUB",
  //     departureTime: dTime,
  //     returnTime: rTime,
  //     oppositeDirectionLiftId: '',
  //     isActive: true,
  //     driverEnRoute: false,
  //     isComplete: true
  //   } //
  //   lId = Lifts.insert(lift);
  // },
  //
  // createFixturesUser: function() {
  //   var fname = faker.name.firstName();
  //   var lname = faker.name.lastName();
  //   var fullname = fname + " " + lname;
  //   var em = faker.internet.email();
  //   var prof = {
  //     // facebookID: 000000000000001,  //for testing, but doesn't really do anything helpful
  //     address : "123 Main St., Bozeman, MT 59715",
  //     birthDate : faker.date.past(),
  //     age : null,
  //     phone: "4061234567",
  //     skierOrBoarder : "",
  //
  //     vehicle : {
  //         make : "Subaru",
  //         model : "Outback",
  //         year : 2001,
  //         seats : 4,
  //         color : "Red",
  //         MPG : 35
  //       },
  //     settings : {
  //         defaultDestination : 1,
  //         defaultPickupLocation : "123 Main St., Bozeman, MT 59715",
  //         defaultDepartureTime : null,
  //         preferredAgeRangeMin : 18,
  //         preferredAgeRangeMax : 25,
  //         defaultCity : ""
  //       },
  //     numRequestsSent : 0,
  //     hasOnboarded : true,
  //     currentLift: {
  //       liftId : null,
  //       departureTime : null,
  //       returnTime : null,
  //       liftRole : null
  //     },
  //   };
  //
  //   var user = Accounts.createUser({
  //     email: em,
  //     password: "sharelift",
  //     profile: prof
  //   });
  //
  //   //fake the data that will be pulled from fb
  //   Meteor.users.update({
  //     _id: user
  //   }, {
  //     $set: {
  //       "services.facebook": {
  //         email: em,
  //         first_name: fname,
  //         last_name: lname,
  //         name: fullname,
  //         gender: "male", //idk how to do random genders with faker.js; yolo
  //         locale: "en_US"
  //       }
  //     }
  //   });
  //
  //   user = Meteor.users.findOne({
  //     _id: user
  //   }); //get actual user object
  //   return user;
  // },
});
// insertFixtureOpenLiftRiderFarOffDude
// insertFixtureOpenLiftDriverFarOffDude

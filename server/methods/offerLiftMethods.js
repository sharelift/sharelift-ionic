Meteor.methods({

  offerLift: function(offer) {

    // var isValid = Match.test(offer, offerLiftSchema); //this is really fricked in the a and not working
    // console.log(isValid);
    // if(!isValid) {
    // 	console.log("shit ain't right");
    // 	return;
    // }

    //Right now the user is automatically given the role of driver
    Roles.addUsersToRoles(Meteor.userId(), ["driver"]);

    console.log(" ===  OFFER LIFT METHOD CALLED === ");
    console.log(offer);

    if(!Meteor.userId()) {
      console.log("user not logged in");
      return;
    }
    if(!Roles.userIsInRole(Meteor.userId(), ['driver'])) {
      console.log("ain't driver, can't post lift");
      return;
    }
//need to change this so that it checks if the user already has an open lift AT THE SAME TIME, not an open lift period.

    // if(Lifts.find({driverId : Meteor.userId(), isComplete : false}).count() !== 0) {
    //   console.log("user already has an open lift, but for now WE AINT GIVIN FUCKS. Change this code.");
    //   // if the departure time of the lift is between the departureTime and departureTime + estimatedTravelTime of a different lift they are confirmed in, don't allow them to offer one for this time.
    //
    //   //put all of this (above) in the request lift method too. How to do callbacks? This should really happen on the front-end...
    //   //return;
    // }
    //use if(isConfirmedInOtherLiftsAtSameTime)

    // if(Meteor.call('isConfirmedInOtherLiftsAtSameTime')){
    //   return;
    // }

    var estTravelTime = 60;  //the estimated travel time, in minutes. Eventually should be determined via a Google API call, but for now it's hard-coded
    // var returnTime = moment(offer.returnTime).toDate();



    var exit = false;
    Meteor.call('isConfirmedInOtherLiftsAtSameTime', offer.departureTime, estTravelTime, function(error, result){
      if(error){
        alert("error!");
      }
      else if(result == true){
        exit = true;  //if there the user is in a lift at the same time, set exit to true
      }
    });

    if(exit == true){
      console.log("==== HAS LIFT AT SAME TIME. EXITING METHOD =====");
      return false;
    }

    //delete the driver's lift requests for lifts that will be active during the departure time
    Meteor.call('dismissRequestsForLiftsAtSameTime', offer.departureTime);

    var curTime = moment();
    var liftDate = moment(offer.departureTime);
    var departureTime = moment(offer.departureTime).toDate();


    //Set the values of startEndLocation and endLocation based on which direction the lift is going
    var calculatedStartLocation;
    var calculatedEndLocation;

    // query info for the destination location object
    let destId = parseInt(offer.destinationId); //offer.destinationId is a string, so convert it to an int
    let locationObject = Locations.findOne({locationId: destId}); //the location object of the destination
    let destLocation  = {
      fullAddress: locationObject.street1 + " " + locationObject.street2 + " "+ locationObject.city + ", " + locationObject.state + " " + locationObject.zip,
      vicinity: locationObject.destinationName,
      lat: locationObject.latitude,
      long: locationObject.longitude
    };


    //if it's a fromDestination lift, set the start location = address of destination
    if(offer.liftDirection == 'fromDestination'){
      calculatedStartLocation = destLocation;
      calculatedEndLocation = offer.startEndLocation;
    }
    //if it's "toDestination" do the opposite
    else{
      calculatedStartLocation = offer.startEndLocation;
      calculatedEndLocation = destLocation;
    }

    // run points through the Google Matrix api to get time & distance
    let start = {};
    start.lat = calculatedStartLocation.lat;
    start.long = calculatedStartLocation.long;
    let end = {};
    end.lat = calculatedEndLocation.lat;
    end.long = calculatedEndLocation.long;

    let routeInfo;
    Meteor.call('getMatrixTimeDistance', start, end, function(error, result){
      if(error){
        alert("error!");
      }
      else{
        routeInfo = result;
        // console.log(" -- getMatrixTimeDistance called. The result is: --");
        // console.log(routeInfo);
        //do something with it...
      }
    });
    let durationInSeconds = routeInfo.duration.value;
    let calculatedArrivalTime = moment(departureTime).add(durationInSeconds, 'seconds').toDate();


    var name = Meteor.user().services.facebook.first_name + " " + Meteor.user().services.facebook.last_name;
    lift = {
      driverId : Meteor.userId(),
      inputTime : curTime.toDate(),
      driverName : name,
      driverPhone: Meteor.user().profile.phone,
      seats : offer.seats,
      date: liftDate, //a moment object
      potentialRiders : [],
      confirmedRiders : [],
      deniedRiders: [],
      currentRiders: [],
      liftDirection : offer.liftDirection,
      destinationLocationId : parseInt(offer.destinationId),
      startEndLocation : calculatedStartLocation, //this needs to be a complete address object, which INCLUDES LAT & LONG
      endLocation : calculatedEndLocation,

      startSector : 'startSect',
      estimatedTravelTime: estTravelTime, // estimated time in minutes. Currently just set to a fixed number (60 min)
      estimatedArrivalTime : calculatedArrivalTime, // a date object, calculated initially. Should be updated as riders are confirmed.
      ifNoRidersGoogleRouteInfo : routeInfo,  //the time and duration information if the driver were to pick up NO RIDERS
      distanceTraveled: 0,  //starts at 0, but will need to be updated upon lift completion.
      departureTime : departureTime,
      returnTime : '',
      oppositeDirectionLiftId : '',
      isActive : false,
      driverEnRoute : false,
      hasArrivedAtDestination: false, //true when the vehicle arrives at the destination (only for toDestination lifts)

      estimatedCost : 0,
      fees : 0,
      payments : [],
      driverPayment : 0,

      isComplete : false,
      offerId: offer.offerId,
    };
    lId = Lifts.insert(lift);

    var curLift = Lifts.findOne({_id : lId})

    if(lift.offerId){

      let requestObj = RideRequests.findOne({_id: lift.offerId})

      let requestOffer = {
        _id: lId,
        driverId: lift.driverId,
        driverName: lift.driverName,
        liftDirection: lift.liftDirection,
        date: lift.date,
        destinationLocationId: lift.destinationLocationId,
        startEndLocation: lift.startEndLocation,
        endLocation: lift.endLocation,
        departureTime: lift.departureTime
      }

      RideRequests.update({
        _id: lift.offerId
      }, {
        $push: {
          offers: requestOffer
        }
      });

      // --VVVVVVVVVVVVV-- THIS IS WHERE NOTIFICATIONS SHOULD BE SENT TO THE USER WHO POSTED THE REQUEST THAT SOMEONE HAS OFFERED THEM A LIFT
      // {{requestObj}} is the request object, {{lift}} is the lift object

      var now = new Date();
      Notifications.insert({
        driverId: lift.driverId,
        riderId: requestObj.riderId,
        liftId: lId,
        requestId: lift.offerId,
        userName : lift.driverName,
        read: false,
        notifType: "sendOffer",
        date: now,
        dismissed:false
      });

      var push = {
        notifType : "sendOffer",
        userId : requestObj.riderId,
        userName : lift.driverName,
      }
      Meteor.call("sendPushNotification", push);


    }

    console.log("NEWLY CREATED LIFT LOOKS LIKE THIS:");
    console.log(lift);
    //
    console.log("THE USER LOOKS LIKE THIS:");
    console.log(Meteor.user());

    // IF the user has connected their Facebook account, send notifications to all FB friends
    if(Meteor.user().profile.hasConnectedFacebook && Meteor.user().profile.hasConnectedFacebook == true){
      var destinationName = locationObject.destinationName;
      if(offer.roundTrip == true){
        //just send one push notification
        if(offer.liftDirection == "fromDestination"){
          var liftAlertText = "Your Facebook friend " + Meteor.user().services.facebook.name + " just posted a lift to and from " + destinationName;
          // console.log("SENDING PUSH NOTIFICATION:");
          // console.log(liftAlertText);
          Meteor.call("sendFacebookFriendLiftAlert", liftAlertText);
        }

      }
      else{
        if(offer.liftDirection == "toDestination"){
          var liftAlertText = "Your Facebook friend " + Meteor.user().services.facebook.name + " just posted a lift from " + offer.startEndLocation.vicinity + " to " + destinationName;
        }
        else{ //if fromDestination
          var liftAlertText = "Your Facebook friend " + Meteor.user().services.facebook.name + " just posted a lift from " + destinationName + " to " + offer.startEndLocation.vicinity;
        }
        // console.log("SENDING PUSH NOTIFICATION:");
        // console.log(liftAlertText);
        Meteor.call("sendFacebookFriendLiftAlert", liftAlertText);
      }
    }


    console.log(" -- offerLift method complete, returning TRUE");
    return true;
  },




  createRequest: function(request) {

    // var isValid = Match.test(offer, offerLiftSchema); //this is really fricked in the a and not working
    // console.log(isValid);
    // if(!isValid) {
    // 	console.log("shit ain't right");
    // 	return;
    // }

    //Right now the user is automatically given the role of driver

    console.log(" ===  CREATE REQUEST METHOD CALLED === ");
    console.log(request);

    if(!Meteor.userId()) {
      console.log("user not logged in");
      return;
    }
    // if(!Roles.userIsInRole(Meteor.userId(), ['driver'])) {
    //   console.log("ain't driver, can't post lift");
    //   return;
    // }
//need to change this so that it checks if the user already has an open lift AT THE SAME TIME, not an open lift period.

    // if(Lifts.find({driverId : Meteor.userId(), isComplete : false}).count() !== 0) {
    //   console.log("user already has an open lift, but for now WE AINT GIVIN FUCKS. Change this code.");
    //   // if the departure time of the lift is between the departureTime and departureTime + estimatedTravelTime of a different lift they are confirmed in, don't allow them to offer one for this time.
    //
    //   //put all of this (above) in the request lift method too. How to do callbacks? This should really happen on the front-end...
    //   //return;
    // }
    //use if(isConfirmedInOtherLiftsAtSameTime)

    // if(Meteor.call('isConfirmedInOtherLiftsAtSameTime')){
    //   return;
    // }

    var estTravelTime = 60;  //the estimated travel time, in minutes. Eventually should be determined via a Google API call, but for now it's hard-coded
    // var returnTime = moment(offer.returnTime).toDate();



    var exit = false;

    //Sees if there is any confirmed lifts within the time slot
    Meteor.call('isConfirmedInOtherLiftsWithinSameTime', request.startDepartureTime, request.endDepartureTime, function(error, result){
      if(error){
        throw new Meteor.Error(error);
      }
      else if(result == true){
        exit = true;  //if there the user is in a lift at the same time, set exit to true
      }
    });

    if(exit == true){
      console.log("==== HAS LIFT AT SAME TIME. EXITING METHOD =====");
      return false;
    }

    //delete the driver's lift requests for lifts that will be active during the departure time

    //NEED TO ADD FEATURE TO REMOVE OTHER CONFLICTING RIDE REQUESTS

    //Meteor.call('dismissRequestsForLiftsAtSameTime', offer.departureTime);

    var curTime = moment();
    var liftDate = moment(request.startDepartureTime);
    var startDepartureTime = moment(request.startDepartureTime).toDate();


    //Set the values of startEndLocation and endLocation based on which direction the lift is going
    var calculatedStartLocation;
    var calculatedEndLocation;

    // query info for the destination location object
    let destId = parseInt(request.destinationId); //request.destinationId is a string, so convert it to an int
    let locationObject = Locations.findOne({locationId: destId}); //the location object of the destination
    let destLocation  = {
      fullAddress: locationObject.street1 + " " + locationObject.street2 + " "+ locationObject.city + ", " + locationObject.state + " " + locationObject.zip,
      vicinity: locationObject.destinationName,
      lat: locationObject.latitude,
      long: locationObject.longitude
    };


    //if it's a fromDestination lift, set the start location = address of destination
    if(request.liftDirection == 'fromDestination'){
      calculatedStartLocation = destLocation;
      calculatedEndLocation = request.startEndLocation;
    }
    //if it's "toDestination" do the opposite
    else{
      calculatedStartLocation = request.startEndLocation;
      calculatedEndLocation = destLocation;
    }


    var name = Meteor.user().services.facebook.first_name + " " + Meteor.user().services.facebook.last_name;

    request = {
      riderId : Meteor.userId(),
      inputTime : curTime.toDate(),
      riderName : name,
      riderPhone: Meteor.user().profile.phone,
      date: liftDate, //a moment object
      liftDirection : request.liftDirection,
      destinationLocationId : parseInt(request.destinationId),
      startEndLocation : calculatedStartLocation, //this needs to be a complete address object, which INCLUDES LAT & LONG
      endLocation : calculatedEndLocation,
      startDepartureTime : request.startDepartureTime, //not a moment object
      endDepartureTime : request.endDepartureTime, //not a moment object
      offers: [],
      isAccepted : false
    };
    console.log(request)
    RideRequests.insert(request);

    // console.log("NEWLY CREATED LIFT LOOKS LIKE THIS:");
    // console.log(lift);
    //
    // console.log("THE USER LOOKS LIKE THIS:");
    // console.log(Meteor.user());

    var destinationName = locationObject.destinationName;

    // if(offer.roundTrip == true){
    //   //just send one push notification
    //   if(offer.liftDirection == "fromDestination"){
    //     var liftAlertText = "Your Facebook friend " + Meteor.user().services.facebook.name + " just posted a lift to and from " + destinationName;
    //     // console.log("SENDING PUSH NOTIFICATION:");
    //     // console.log(liftAlertText);
    //     Meteor.call("sendFacebookFriendLiftAlert", liftAlertText);
    //   }

    // }
    // else{
    //   if(offer.liftDirection == "toDestination"){
    //     var liftAlertText = "Your Facebook friend " + Meteor.user().services.facebook.name + " just posted a lift from " + offer.startEndLocation.vicinity + " to " + destinationName;
    //   }
    //   else{ //if fromDestination
    //     var liftAlertText = "Your Facebook friend " + Meteor.user().services.facebook.name + " just posted a lift from " + destinationName + " to " + offer.startEndLocation.vicinity;
    //   }
    //   // console.log("SENDING PUSH NOTIFICATION:");
    //   // console.log(liftAlertText);
    //   Meteor.call("sendFacebookFriendLiftAlert", liftAlertText);
    // }
    console.log("==== SUCCEEDED =====");
    return true;
  },




  //Checks if the user is already confirmed (as a driver or rider) in a lift at this time
  isConfirmedInOtherLiftsAtSameTime: function(liftDepartureTime, liftTravelTime) { //date is a timestamp, not a Moment object.
    // console.log("isConfirmedInOtherLiftsAtSameTime called!");
    var liftArrivalTime = moment(liftDepartureTime).add(liftTravelTime, 'minutes').toDate();

    //fetch all lifts that the user has requested that have a departure time on the same day as the lift they are posting
    let confirmedLifts = Lifts.find({ $or: [{"confirmedRiders.userId": Meteor.userId()}, {driverId: Meteor.userId()}], isComplete: false}).fetch();

    for(let i = 0; i<confirmedLifts.length; i++){
      var iArrivalTime = moment(confirmedLifts[i].departureTime).add(confirmedLifts[i].estimatedTravelTime, 'minutes').toDate();
      //if a given lift overlaps with the lift that is being offered, return true.
      if(liftArrivalTime >= confirmedLifts[i].departureTime && liftDepartureTime <= iArrivalTime){
        // console.log("returning true - there is a conflict");
        console.log(" -- isConfirmedInOtherLiftsAtSameTime finished: returning TRUE --");
        return true;

      }

    }
    // console.log("returning false");
    console.log(" -- isConfirmedInOtherLiftsAtSameTime finished: returning FALSE --");
    return false;


  },


  isConfirmedInOtherLiftsWithinSameTime: function(requestStartTime, requestEndTime) { //date is a timestamp, not a Moment object.
    // console.log("isConfirmedInOtherLiftsAtSameTime called!");

    //fetch all lifts that the user has requested that have a departure time on the same day as the lift they are posting
    let confirmedLifts = Lifts.find({ $or: [{"confirmedRiders.userId": Meteor.userId()}, {driverId: Meteor.userId()}], isComplete: false}).fetch();

    for(let i = 0; i<confirmedLifts.length; i++){
      //if a given lift overlaps with the lift that is being offered, return true.

      if(confirmedLifts[i].startDepartureTime < requestStartTime && confirmedLifts[i].endDepartureTime > requestStartTime){
        return true;
      } else if(confirmedLifts[i].startDepartureTime < requestEndTime && confirmedLifts[i].endDepartureTime > requestEndTime){
        return true;
      } else if(confirmedLifts[i].startDepartureTime < requestStartTime && confirmedLifts[i].endDepartureTime > requestEndTime){
        return true;
      } else if(confirmedLifts[i].startDepartureTime > requestStartTime && confirmedLifts[i].endDepartureTime < requestEndTime){
        return true;
      }

    }
    // console.log("returning false");
    return false;

  },


  //Checks if the user has requested lifts on the same day of the departure time. Not using it currently.
  // hasRequestedOtherLiftsOnThisDay: function(date) { //date is a timestamp, not a Moment object.
  //
  //   var start = new Date(date.getFullYear(), date.getMonth(), date.getDate());  //start = 00:00 AM for the given day
  //   var end = new Date(date.getFullYear(), date.getMonth(), date.getDate()+1) //end = 00:00 AM the following day
  //
  //   //fetch all lifts that the user has requested that have a departure time on the same day as the lift they are posting
  //   let potentialLifts = Lifts.find({$and: [{departureTime: {$gte: start}}, {departureTime: {$lt: end}}, {"potentialRiders.userId":Meteor.userId()}]}).fetch();
  //   // console.log("The number of lifts you've already requested for this day are:");
  //   // console.log(potentialLifts.length);
  //
  //   if(potentialLifts.length > 0){  //if the user has requested lifts for the same day they are posting a lift for
  //     return true;
  //   }
  //   else{ //the user has not requested any lifts for the same day they are posting a lift for
  //     return false;
  //   }
  // },

  dismissRequestsForLiftsAtSameTime: function(liftDepartureTime){ //deletes lift requests for lifts that have a departure time on the same day as the lift they are posting

    //fetch all lifts that the user has requested that have a departure time on the same day as the lift they are posting
    let potentialLifts = Lifts.find({"potentialRiders.userId":Meteor.userId(), isComplete: false}).fetch();

    for(let i = 0; i<potentialLifts.length; i++){
      var estArrivalTime = moment(potentialLifts[i].departureTime).add(potentialLifts[i].estimatedTravelTime, 'minutes').toDate();
      //for all lift requests for lifts that fall between the
      // new lift's departureTime and estArrivalTime, dismiss the requests:
      if(liftDepartureTime >= potentialLifts[i].departureTime && liftDepartureTime <= estArrivalTime){
        let liftData = potentialLifts[i];

        //dismiss request notification from Driver
        let notifs = Notifications.find({liftId:liftData._id, riderId:Meteor.userId()}).fetch();
        for(let i = 0; i<notifs.length; i++){
              Notifications.update({_id : notifs[i]._id}, {$set: {dismissed: true}}); //Iterate through and dismiss them.
        }

        Lifts.update({_id: liftData._id}, {$pull: {potentialRiders: {userId: Meteor.userId()} } }); //Pull rider from the potentialRiders array

        //Give the user back a request
        Meteor.users.update(Meteor.userId(), {$inc: {"profile.numRequestsSent": -1,}}); // GIve them back their requests

      }
    }

    console.log(" -- dismissRequestsForLiftsAtSameTime finished --");
  }



});

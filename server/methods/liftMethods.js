Meteor.methods({

  updateLiftState: function(liftId, stateName, value){

    if(stateName == "isActive"){
      Lifts.update({_id: liftId}, {$set: {isActive: value } });
    }
    else if(stateName == "driverEnRoute"){
      Lifts.update({_id: liftId}, {$set: {driverEnRoute: value } });
    }
    else if(stateName == "hasArrivedAtDestination"){
      Lifts.update({_id: liftId}, {$set: {hasArrivedAtDestination: value } });
    }
    else if(stateName == "isComplete"){
      Lifts.update({_id: liftId}, {$set: {isComplete: value } });
    }
    else{
      console.error("Error with updateLiftState Meteor method: the stateName parameter doesn't exist.");
    }
    // console.log(Lifts.findOne({_id: liftId}));

  },

  submitPayment: function(paymentModel){
    //the payment object
    let payment = {
      fromUserId: paymentModel.fromUserId,
      toUserId: paymentModel.toUserId,
      liftId: paymentModel.liftId,  //the lift associated with the payment
      paymentOption: paymentModel.paymentOption,  //for now, this means in what way the rider decided to pay: suggested, custom, or noPay
      amount: paymentModel.amount
    };

    console.log(" --- submitPayment called ---");
    console.log(payment);

    //add the new payment to the Payments collection
    var paymentId = Payments.insert(payment);
    console.log(paymentId);

    //insert the new payment into the correct lift
    let thisPayment = Payments.findOne({_id: paymentId});
    console.log(thisPayment);

    Lifts.update(
      {_id: paymentModel.liftId},
      { $push: { payments: thisPayment } }
    );

    console.log("-- PAYMENT CREATED. The lift looks like this: -- ");
    console.log(Lifts.findOne({_id: paymentModel.liftId}));


    // return true if all goes well
    return true;

  },

  confirmRiderPickupDropoff: function(liftId, riderObject){
    Lifts.update(
      {_id: liftId, confirmedRiders: { $elemMatch: { userId: riderObject.userId } }},
      {$push: { currentRiders: riderObject } }
    );
    Lifts.update(
      {_id: liftId, confirmedRiders: { $elemMatch: { userId: riderObject.userId } }},
      {$set: {"confirmedRiders.$.pickupDropOffConfirmed": true}}
    );

    let thisLift = Lifts.findOne({_id: liftId});
    console.log(thisLift);
    console.log("== confirmRiderPickupDropoff ==");

    //if the lift is toDestination, send a notification to the rider that is being picked up:
    if(thisLift.liftDirection == 'toDestination'){
      let push = {
        userId : riderObject.userId,
        notifText : "Your ShareLift driver " + thisLift.driverName + " is here to pick you up!",
      }
      Meteor.call("sendSinglePushNotification", push);

      // send an in-app notification
      Notifications.insert({
        driverId: thisLift.driverId,
        riderId: riderObject.userId,
        liftId: liftId,
        userName: riderObject.userName,
        read: false,
        notifType: "driverIsHere",
        date: new Date(),
        notifText: push.notifText,
        dismissed:false
      });

    }




  },

  //sends push & in-app notifications to riders when the driver starts the lift
  startLiftCheckpointNotifications: function(liftId){
    // Send a push notification to passengers
    let confirmedRiderIDs = [];  //an array of Meteor IDs to send push notifications to
    let thisLift = Lifts.findOne({_id: liftId});
    let notifText = "Your ShareLift driver " + thisLift.driverName + " is on their way. Be ready!";
    thisLift.confirmedRiders.forEach(function (riderData) {
      confirmedRiderIDs.push(riderData.userId);

      Notifications.insert({
        driverId: thisLift.driverId,
        riderId: riderData.userId,
        liftId: thisLift._id,
        userName: riderData.userName,
        read: false,
        notifType: "driverOnTheWay",
        date: new Date(),
        notifText: notifText,
        dismissed:false
      });
    });


    let pushData = {
      userIds : confirmedRiderIDs,
      notifText : notifText,
    }

    Meteor.call('sendMultiplePushNotifications', pushData);

  }

  // db.lifts.update(
  //   {_id: "Jdfpypq5jtpfMoHH8", confirmedRiders: { $elemMatch: { userId: "h4usMuWErdJccGXE9" } }},
  //   {$set: {"confirmedRiders.$.testBoolean": false}}
  //
  // )



});

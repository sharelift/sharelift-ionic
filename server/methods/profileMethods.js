Meteor.methods({

  // exampleMethod: function() {
  //
  // },

  uploadProfilePhoto: function(data) {
    //check the data type?

    //The user has to be logged in
    if(!Meteor.userId()) {
      // throw new Meteor.Error('not-logged-in',
      //   'Must be logged in to update his picture.');
      console.log("user not logged in");
      return;
    }
    Meteor.users.update(Meteor.userId(), {
      $set: {
        "profile.profilePicture": data
      }
    });
  },

  uploadVehiclePhoto: function(data) {
    //check the data type?
    console.log(" == uploadVehiclePhoto called == ");
    console.log(data);

    //The user has to be logged in
    if(!Meteor.userId()) {
      // throw new Meteor.Error('not-logged-in',
      //   'Must be logged in to update his picture.');
      console.log("user not logged in");
      return;
    }
    Meteor.users.update(Meteor.userId(), {
      $set: {
        "profile.vehicle.vehiclePicture": data
      }
    });
  },

  addFacebookInfoToUser: function(data){
      //fake the data that will be pulled from fb
      Meteor.users.update(
        {_id : Meteor.userId()},
        {$set : {
          "services.facebook" : {
            email : data.email,
            first_name : data.firstName,
            last_name : data.lastName,
            name : data.fullName,
            gender : data.gender,
            locale : data.locale
          }
        }});
  },

  saveCardInfo: function(cardInfo){
    console.log("-- addCardInfo called --");

//The structure of cardInfo:
    // cardInfo = {
    //   number: ccNum,
    //   cvc: cvc,
    //   exp_month: expMo,
    //   exp_year: expYr
    // }

    Meteor.users.update(
      {_id : Meteor.userId()},
      {$push : {
        "profile.payment.cards" : {
          number : cardInfo.number,
          cvc : cardInfo.cvc,
          exp_month : cardInfo.exp_month,
          exp_year : cardInfo.exp_year,
          name : "Placeholder Name",
          cardTitle : "Placeholder Card Title",
          dateAdded : new Date()
        }
      }});

    console.log("-- Card info added! --");
    console.log(Meteor.users.findOne({_id: Meteor.userId()}));



  },

  updateCardInfo: function(cardInfo){
    console.log("-- updateCardInfo called --");
    // console.log(cardInfo);

    Meteor.users.update(
      {_id : Meteor.userId(), "profile.payment.cards.number": cardInfo.oldNumber, "profile.payment.cards.cvv": cardInfo.oldCvv},
      {$set : {
        "profile.payment.cards.$" : { //the $ sign points to the specific card queried above ("profile.payment.cards.number": cardInfo.oldNumber)
          number : cardInfo.number,
          cvc : cardInfo.cvc,
          exp_month : cardInfo.exp_month,
          exp_year : cardInfo.exp_year,
          name : "Placeholder Name",
          cardTitle : "Placeholder Card Title",
          dateUpdated : new Date()
        }
      }});

    // console.log("-- Card updated! --");
    // console.log(Meteor.users.findOne({_id: Meteor.userId()}));

  }







});

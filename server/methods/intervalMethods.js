Meteor.setInterval(function(){
  console.log("test");
  Meteor.call('markPastLiftsAsComplete');
}, 1800000);  // triggers every 30 min


Meteor.methods({

  markPastLiftsAsComplete : function(){
    console.log("markPastLiftsAsComplete called");

    let cutoffTime = moment(new Date()).subtract(12, 'hours').toDate(); // the cutoff time is 12 hours ago
    // console.log("THE CUTOFF TIME");
    // console.log(cutoffTime);

    // if lifts exist whos departure times are greater than 12 hours in the past
    // and are still marked as isComplete: false, find them and mark them as complete

    // let oldLifts = Lifts.find({isComplete: false, departureTime: {$lt: cutoffTime} }).fetch(); //for testing

    console.log("Getting rid of old lifts. Setting the following to isComplete: ");
    // console.log(oldLifts);
    Lifts.update(
      {isComplete: false, departureTime: {$lt: cutoffTime} },
      {$set:
        { isComplete: true }
       }
     );

  }

});

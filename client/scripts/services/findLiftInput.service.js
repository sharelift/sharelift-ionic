angular
  .module('Sharelift')
  .service('findLiftInputService', findLiftInputService);

function findLiftInputService(){
  // let findLiftInputService = this;

  this.pickupDropoffLocation = Meteor.user().profile.settings.defaultPickupLocation || null;  //initially no pickupDropoffLocation is set

//   --- For testing only. Including this will mean users wont have to select a location before viewing lifts! ---
  //  this.pickupDropoffLocation = {
  //    //a default/fake location
  //    fullAddress: "1234 Filler Address, Bozeman, MT",
  //    vicinity: "A city",
  //    lat: 45.6700000,
  //    long: 111.04000000000000
  //  };

   this.setPickupDropoffLocation = function(newLocation){
      console.log("-- this.setPickupDropoffLocation called! Inputting: -- ");
      console.log(newLocation);
      this.pickupDropoffLocation = newLocation;
      if(!Meteor.user().profile.settings.defaultPickupLocation){
        console.log('This works')
        Meteor.users.update(Meteor.userId(), {$set : {'profile.settings.defaultPickupLocation' : newLocation} });
      }
   }

   this.getPickupDropoffLocation = function(){
     console.log("-- this.getPickupDropoffLocation called! Returning: -- ");
     console.log(this.pickupDropoffLocation);
     return this.pickupDropoffLocation;
   }


}

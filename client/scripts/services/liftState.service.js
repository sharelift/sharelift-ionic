angular
  .module('Sharelift')
  .service('liftStateService', liftStateService);

function liftStateService(){

  //determines if the user is a confirmed rider in the lift (uses this.data)
  // this.isConfirmedRiderInThisLift = function(lift){
  //   let ret = false;
  //   if(Meteor.user()){
  //     let myLift = Lifts.findOne({_id: lift.liftId});
  //     if(myLift){
  //       for(let i = 0; i< myLift.confirmedRiders.length; i++){
  //         if(myLift.confirmedRiders[i].userId == Meteor.userId()){
  //           // the user is a confirmedRider
  //             ret = true;
  //             $scope.isInThisLift = true;
  //         }
  //       }
  //     }
  //   }
  //   return ret;
  //
  // }

  // Which isConfirmedRiderInThisLift method is better? (above or below?) Query the database or use confirmedRiders array?

  this.isConfirmedRiderInThisLift = function(lift){
    let ret = false;
    for(let i = 0; i < lift.confirmedRiders.length; i++){
      if(lift.confirmedRiders[i].userId == Meteor.userId()){
        ret = true;
      }
    }
    return ret;
  }

  this.isPotentialRiderInThisLift = function(lift){
    let ret = false;
    for(let i = 0; i < lift.potentialRiders.length; i++){
      if(lift.potentialRiders[i].userId == Meteor.userId()){
        ret = true;
      }
    }
    return ret;
  }

  this.hasOfferBeenSent = function(request){
    let ret = false;
    for(let i = 0; i < request.offers.length; i++){
      if(request.offers[i].userId == Meteor.userId()){
        ret = true;
      }
    }
    return ret;
  }

  this.isDriverInThisLift = function(lift){
    if(Meteor.userId() == lift.driverId){
      return true;
    }
    else{
      return false;
    }
  }

  this.liftIsInPastWithoutRiders = function(lift){
    // console.log("=== liftIsInPastWithoutRiders called === The lift object looks like this:");
    // console.log(lift);
    if(!this.liftHasConfirmedRiders(lift) && this.isLiftInThePast(lift)){
      return true;
    }
    else{
      return false;
    }
  };


  this.liftHasConfirmedRiders = function(lift){
    // console.log("=== liftHasConfirmedRiders called ===");
    let ret = false;
    if(lift){
      if(lift.confirmedRiders.length > 0){
        ret = true;
      }
    }
    // console.log("returning: " + ret);
    return ret;
  };

  this.isLiftInThePast = function(lift){
    // console.log(" ==  isLiftInThePast called ==");
    let ret = false;
    if(lift && lift.departureTime ){

      //sets how far after the said 'departure time' the lift is considered "in the past"
      let cutoffTime = moment(lift.departureTime).add(60, 'minutes').toDate(); //set to 1 hour after the posted departure time
      let now = new Date();
      // console.log("cutoffTime = " + cutoffTime);
      if(now > cutoffTime){ //If it's past the cutoff time
        ret = true;
        // console.log("returning: " + ret);
      }

    }
    return ret;
  };


}

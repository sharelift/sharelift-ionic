angular
  .module('Sharelift')
  .controller('DriverSignupCtrl', DriverSignupCtrl);

function DriverSignupCtrl ($scope, $state, $meteor, photoUploadService) {

  $scope.driverModel = {};



//on submit, go to main $state.go('Main.ride');

  $scope.submit = function(){
    console.log("Form submitted");
    console.log($scope.driverModel);

    Meteor.call('updateProfileVehicle', Meteor.userId(), $scope.driverModel);
    console.log("Added vehicle data to database");


    $state.go('Main.ride');


  }

  $scope.takeVehiclePhoto = function(){
    $meteor.getPicture().then(function(data){
      //console.log(data);
      Meteor.call('uploadVehiclePhoto', data);
    });
    //  Use this if we get AWS S3 working
    // photoUploadService.uploadVehiclePhoto();
  }
}

angular
.module('Sharelift')
.controller('RequestViewCtrl', RequestViewCtrl);

function RequestViewCtrl ($scope, $reactive, $stateParams, $meteor, $ionicModal, $timeout, $log, $state, $controller, $rootScope, $location, $ionicActionSheet, findLiftInputService, liftStateService, uiGmapGoogleMapApi, $ionicHistory) {
  $reactive(this).attach($scope);
  var popupController = $controller('PopupCtrl', {$scope: $scope});


  //gives access to liftStateService variables and functions within HTML, use $root.stateService._____ to access.
  $rootScope.stateService = liftStateService;
  $rootScope.findLiftInputService = findLiftInputService;
  $scope.navTitle='<img class="title-image" src="img/sharelift_elementsV1_4.svg" width="100"/>';

  //Subscriptions
  this.subscribe('getLocations');
  // this.subscribe('getLocations');
  this.subscribe('getUserData');
  this.subscribe('getLiftRatings');
  // this.subscribe('getRiderNotificationsConfirm');
  // this.subscribe('getRiderNotificationsDeny');
  this.subscribe('getAllRiderNotifications');
  this.subscribe('getPastLiftsDriver');
  this.subscribe('getPastLiftsRider');
  this.subscribe('getDriverNotifications');
  this.subscribe('allUserData');  //subscribes to all public data for all users - used for profile photos

  this.subscribe('liftsWhereRider');  //gets data for lifts where user is a rider
  this.subscribe('liftsWhereDriver'); //gets data for lifts where user is a driver
  this.subscribe('getOpenRequests'); //gets open requests



  this.requestId = $stateParams.requestId;
  let request = RideRequests.findOne({_id : $stateParams.requestId})

  // Scope level variables
  $scope.requestLiftButtonDisabled = false;
  $scope.liftIsUnrequestable = false;
  $scope.isInThisLift = false;


  // Scope level STATE variables
  var thisRequest = RideRequests.findOne({_id: $stateParams.requestId});

  this.helpers({
    // the entire lift object - super useful...
    data() {
      return RideRequests.findOne({_id : $stateParams.requestId});
    },

  });


  $ionicModal.fromTemplateUrl('client/templates/offerLift.html', {
    scope: $scope,
    animation: 'slide-in-up',
  }).then(function(modal) {
    $scope.offerLiftModal = modal;
  });


  $scope.openOfferLiftModal = function() {
    if(Meteor.users.findOne({_id:Meteor.userId()})){
      let user = Meteor.users.findOne({_id:Meteor.userId()});
      if(!user.profile.vehicle.make || !user.profile.vehicle.model || !user.profile.vehicle.year){
        popupController.showAlert("Oh snap!", "Looks like you didn't provide your vehicle info! Please go do that first.", function(){
          $state.go('Main.profile');
        });
      }
      else if(!user.profile.phone){
        popupController.showAlert("Oh snap!", "Looks like you didn't provide a phone number! Please go do that first.", function(){
          $state.go('Main.profile');
        });
      }
      else{
        $scope.offerLiftModal.show();
      }
      console.log(user);
    }
  };

  $scope.closeOfferLiftModal = function() {
    $scope.offerLiftModal.hide();
  };

  //Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.offerLiftModal.remove();
  });

  // Execute action on hide modal
  $scope.$on('modal.hidden', function() {
    // Execute action
  });

  // Execute action on remove modal
  $scope.$on('modal.removed', function() {
    // Execute action
  });




  $scope.printOut = function(input){
    console.log(input);
  }


  $scope.getLiftDay = function(request){
    var weekday = new Array(7);
    weekday[0]=  "Sunday";
    weekday[1] = "Monday";
    weekday[2] = "Tuesday";
    weekday[3] = "Wednesday";
    weekday[4] = "Thursday";
    weekday[5] = "Friday";
    weekday[6] = "Saturday";
    let ret;
    let requestDate;
    if(request && request.date){
      if(request.date._d){
          requestDate = new Date(request.date._d);
      }
      else{
          request = new Date(request.date);
      }
      if(requestDate && requestDate.getFullYear()){
        let date1 = new Date();
        var date1_tomorrow = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate() + 1);
        if(date1.getFullYear() == requestDate.getFullYear() && date1.getMonth() == requestDate.getMonth() && date1.getDate() == requestDate.getDate()){
          ret = "Today";
        }
        else if (date1_tomorrow.getFullYear() == requestDate.getFullYear() && date1_tomorrow.getMonth() == requestDate.getMonth() && date1_tomorrow.getDate() == requestDate.getDate()) {
          ret = "Tomorrow"; // date2 is one day after date1.
        }
        else{
          ret = String(weekday[requestDate.getDay()]);
        }
      }
    }
    return(ret);
  }

  $scope.getLiftMonthDay = function(request){
    var month = new Array(12);
    month[0]=  "January";
    month[1] = "February";
    month[2] = "March";
    month[3] = "April";
    month[4] = "May";
    month[5] = "June";
    month[6] = "July";
    month[7] = "August";
    month[8] = "September";
    month[9] = "October";
    month[10] = "November";
    month[11] = "December";
    let ret;
    let requestDate;
    if(request && request.date){
      if(request.date._d){
          requestDate = new Date(request.date._d);
      }
      else{
          requestDate = new Date(request.date);
      }
      ret = month[requestDate.getMonth()] + " " + requestDate.getDate();
    }
    return ret;
  }

  $scope.getLiftTime = function(time){
    return new Date(time).toLocaleTimeString().replace(/([\d]+:[\d]{2})(:[\d]{2})(.*)/, "$1$3")
  }

  $scope.getUserPhone = function(id){
    let ret = "Phone not provided";
    if(id && Meteor.users.findOne({_id:id})){
      let user = Meteor.users.findOne({_id:id});
      if(user.profile.phone){
        ret = String(user.profile.phone);
      }
    }
    return ret;
  }

  $scope.getUserPhone = function(id){
    let ret = "Phone not provided";
    if(id && Meteor.users.findOne({_id:id})){
      let user = Meteor.users.findOne({_id:id});
      if(user.profile.phone){
        ret = String(user.profile.phone);
      }
    }
    return ret;
  }


  $scope.getProfilePicture = function(id){
    let ret = "/profile_filler.png.png";
    if(Meteor.users.findOne({_id:id})){ //FAILS HERE (I think because this controller isn't subscribing to just any user)
      let user = Meteor.users.findOne({_id:id});
      //console.log(user);
      if(user.profile.profilePicture){
        ret = user.profile.profilePicture; //Commented out for now, going to try to find a solution to this issue.
        //console.log(ret);
      }
    }

    return ret;
  }

  $scope.getDestinationName = function(destinationId){
    let ret = "the slopes";
    if(typeof(destinationId) == "number"){
      if(Locations.findOne({locationId:destinationId})){
          ret = Locations.findOne({locationId:destinationId}).destinationName;
      }
    }
    return ret;
  }



  $scope.getDriverVehicleInfo = function(id){
    let ret = "Vehicle not specified"
    if(id && Meteor.users.findOne({_id:id})){
      let user = Meteor.users.findOne({_id:id});
      if(user.profile.vehicle){
          ret = String(user.profile.vehicle.year) + " " + user.profile.vehicle.make + " " + user.profile.vehicle.model;
      }
    }
    return ret;
  }

  $scope.getDriverVehiclePhoto = function(id){
    let ret = "/vehicle.png";

    if(id && Meteor.users.findOne({_id:id})){
      let user = Meteor.users.findOne({_id:id});
      if(user.profile.vehicle.vehiclePicture){
          ret = user.profile.vehicle.vehiclePicture;
      }
    }
    return ret;
  }


  $scope.getDriverPhone = function(id){
    let ret = "Not provided";
    if(id && Meteor.users.findOne({_id:id})){
      let user = Meteor.users.findOne({_id:id});
      if(user.profile.phone){
        ret =user.profile.phone;
      }
    }
    return ret;
  }

  $scope.requestHasOffers = function(request){
    let ret = false;
    if(request){
      if(request.offers.length > 0){
        ret = true;
      }
    }
    return ret;
  }

  $scope.liftHasNoRiders = function(lift){
    let ret = false;
    if(lift){
      if(lift.confirmedRiders.length === 0){
        ret = true;
      }
    }
    return ret;
  }


  // <<<< Rating Methods >>>> I'm not even sure that we are using these...




  $scope.confirmOffer = function(offer, request) {

    console.log(offer)
    console.log(request)

    popupController.showConfirm("Woah there!", "Are you sure you want accept this request?", function(confirm){
      if(confirm){ // If they say OK,
        Meteor.call('confirmOfferRequest', offer, request , function(error, result){
          if(error){
            console.log(error)
            alert(error)
          } else if(result == true){
            $location.path("index/lift/" + offer._id);
          } else {
            popupController.showAlert("Oh snap!", "Looks like that ride is full now", function(){

              //PUT SOMETHING IN HERE TO CORRECT IT

          });
          }
        });
      }
    }, "Yeah", "Nope");
  };

  $scope.denyOffer = function(offer, request) {
    popupController.showConfirm("Woah there!", "Are you sure you want deny this request?", function(confirm){
      if(confirm){ // If they say OK,
        Meteor.call('denyOfferRequest', offer, request);
      }
    }, "Yeah", "Nope");
  };

  this.requestLift = function() {
    let self = this;
    function requestTheLift(){
      $scope.requestLiftButtonDisabled = true;
      request = {};
      request.liftId = self.data._id;
      request.userName = Meteor.user().services.facebook.name;
      request.user = Meteor.user();
      request.pickupDropoffLocation = findLiftInputService.getPickupDropoffLocation();
      request.pickupTime = "";
      request.driverId = self.data.driverId;
      request.driverName = self.data.driverName;
      request.phone = Meteor.user().profile.phone;


      Meteor.call('requestLift', request);

      $scope.requestLiftButtonDisabled = true;
    }

    if($scope.isLiftConflict()){
      popupController.showAlert("Oh snap!", "You're already confirmed in a lift for the same time, destination, and direction. You'll need to bail from that lift to request this one.", function(){})
    }
    else if(!Meteor.user().profile.phone){
      console.log("No phone!");
      popupController.showInputPopup("Oh snap!", "Please provide your phone number first!", function(number){
        console.log(number);
        if(number){
          let num = String(number).replace(/\D/g,''); // Regex to strip out non-numerical values
          console.log(num);
          if(num.length == 10){
            Meteor.call('addProfilePhoneNumber', Meteor.userId(), num);
            requestTheLift();
          }
          else{
              popupController.showConfirm("Oh snap!", "That wasn't a valid phone number. Please try it again.", function(confirm){
                if(confirm){
                    self.requestLift();
                }
              })
          }
        }
      });
    }
    else{
      requestTheLift();
    }
    //confirmation alert
    popupController.showAlert("Lift requested!", "The driver will be notified. Feel free to request some other lifts :)", function(){});
  };

  $scope.deleteRequest = function(request){
      popupController.showConfirm("Woah there!", "Are you sure you want to cancel this request?", function(confirm){
      if(confirm){ //If they say OK,
        $ionicHistory.goBack(-1)
        Meteor.call('deleteRequest', request);// and cancel the lift request.
      }
    }, "Yeah", "Nope");
  }

  $scope.cancelRequest = function(lift){
    popupController.showConfirm("Woah there!", "Are you sure you want to cancel this request?", function(confirm){
      if(confirm){ //If they say OK,
        $location.path("/"); // redirect to home
        Meteor.call('cancelRideRequest', lift.data);// and cancel the lift request.
        $scope.requestLiftButtonDisabled = false;
      }
    }, "Yeah", "Nope");
  }

  $scope.liftHasBeenRequested = function() {
    //this function aint reactive tho
    if(this.data && this.data.potentialRiders && this.data.potentialRiders.length) {
      for(var i = 0; i < this.data.potentialRiders.length; ++i) {
        if(this.data.potentialRiders[i].userId === Meteor.userId()) {
          $scope.requestLiftButtonDisabled = true;
          return true;
        }
      }
    }
  }


//A go at the cancel button:
  $scope.cancelRider = function(liftData) {
    popupController.showConfirm("Woah there!", "Are you sure you want to leave this lift?", function(confirm){
      if(confirm){ // If they say OK,
        $location.path("/"); // redirect to home
        Meteor.call('cancelRider', liftData); // and cancel the lift
      }
    }, "Yeah", "Nope");
  };

  $scope.getCallRef = function(number){
    if(number){
      let num = String(number).replace(/\D/g,''); // Regex to strip out non-numerical values
      num = num.replace(/(\d{3})(\d{3})(\d{4})/, "$1-$2-$3"); // Super duper regex to put dashes back in
      num = "tel:+1-" + num;
      return num;

    }

  }

  $scope.isLiftConflict = function(){
    //use stateParams and a query on the current user to determine if the user
    // is already confirmed in a lift for the same time, destination, and direction. Or maybe just time... (since you just shouldn't be able to request lifts for the same time that you have a confirmed lift)
    return false;
  }

  ///////////////////////////////////////

  // $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
  // viewData.enableBack = true;
  // });









  $scope.timePickerDeparture = {
    inputEpochTime: 32400,  //Optional
    step: 15,  //Optional
    format: 12,  //Optional
    titleLabel: '12-hour Format',  //Optional
    setLabel: 'Set',  //Optional
    closeLabel: 'Close',  //Optional
    setButtonType: 'button-positive',  //Optional
    closeButtonType: 'button-stable',  //Optional
    callback: function (val) {    //Mandatory
      if(val) {
        this.inputEpochTime = val;
        var t = new Date(val * 1000);
        console.log(t);
        $scope.requestOfferModel.departureTime = t;
      }
    }
  };

  function timePickerCallback(val) {
    if (typeof (val) === 'undefined') {
      console.log('Time not selected');
    } else {
      var selectedTime = new Date(val * 1000);
      console.log('Selected epoch is : ', val, 'and the time is ', selectedTime.getUTCHours(), ':', selectedTime.getUTCMinutes(), 'in UTC');
    }
  };



  $scope.riderName = request.name;

  $scope.requestOfferModel = {  //location, dest, date, depart, return, seats
    liftDirection : request.liftDirection,
    startEndLocation : null,
    endLocation : '',
    destinationId : request.destinationLocationId,
    departureTime : "",
    returnTime : "",
    seats : 1,
    roundTrip : false,
    offerId: request._id
  };

  $scope.checkRequestTime = function(model) {
    if(request.startDepartureTime < model.departureTime && model.departureTime < request.endDepartureTime){
      return true;
    } else {
      return false;
    }
  }

  $scope.getDestinationName = function(id){
    let destination = Locations.findOne({locationId: id});
    return destination.destinationName
  }


  $scope.timeChanged = function () {
    $log.log('Time changed to: ' + $scope.mytime);
  };

    // Submit an offer as a driver to invite the riderRequest to their lift
    // Posts a new lift or sends an offer for an existing lift
  $scope.offerLiftRequest = function(form, model) {

    console.log("-- offerLift called --");


    if(form.$valid === true) {
      var currentTime = new Date();
      console.log(model);
      console.log(currentTime);

      console.log(request.startDepartureTime)
      console.log(model.departureTime)
      console.log(request.endDepartureTime)

      //check for time errors
      if(!(request.startDepartureTime <= model.departureTime && model.departureTime <= request.endDepartureTime)){
          popupController.showAlert("Oh snap!", "You have to post a lift that departs within this user's time slot", function(){});
      }
      else if(model.departureTime <= currentTime){
        popupController.showAlert("Oh snap!", "You can't post a lift that departs in the past. Unless you're Doc Brown, but we both know you aren't.", function(){});
      }
      else if(model.roundTrip && model.returnTime < model.departureTime){
          popupController.showAlert("Oh snap!", "Your return time must be after your departure time.", function(){});
      }
      // .add is not a function you can use on a regular Date object (only for Moment objects), so need to change this
      else if(moment(model.departureTime) <= moment(currentTime).add(10, 'minutes').toDate()){
        popupController.showAlert("Oh snap!", "You can't post a lift that departs less than 10 minutes into the future.", function(){});
      }
      else if(!model.destinationId){
        popupController.showAlert("Oh snap!", "You gotta provide a destination, son!", function(){});
      }
      else{

        //Check if the user has requests for lifts at the same time
        let isConflict = $scope.hasLiftRequestsForThisTime(model.departureTime);
        let isReturnTripConflict = false;
        let returnLiftModel;
        let offerConflict = $scope.hasLiftForThisOfferTime();

        //if this is a round-trip lift:
        if(model.roundTrip){
          isReturnTripConflict = $scope.hasLiftRequestsForThisTime(model.returnTime);

          //create a new model for the return lift:
          returnLiftModel = {
            liftDirection : "fromDestination",
            startEndLocation : model.startEndLocation,
            endLocation : '',
            destinationId : model.destinationId,
            departureTime : model.returnTime,
            returnTime : "",
            seats : model.seats,
            roundTrip : true,
            offerId: null
          };
        }

        if(offerConflict){
          popupController.showConfirm("Heads up!", "You already have a lift departing within this rider's time slot. Do you want to offer them a seat in that lift or create a new one?", function(offerSeat, newLift){
            if(offerSeat){
              Meteor.call('requestOfferSeat', request, offerSeat, function(error, result){
                if(error){
                  alert("error");
                } else {
                  $location.path("index/myLifts");
                }
              })
            }
          }, 'Offer Seat', 'Create New Lift');
        } else if(isConflict || isReturnTripConflict){
          popupController.showConfirm("Heads up!", "Posting a lift will cancel any lift requests you've sent to other drivers for the same time. Is that okay?", function(confirm){
            if(confirm){

              Meteor.call('offerLift', model, function(error, result){
                if(error){
                  alert("error!");
                }
                else if(result == true){
                  // if the first lift is created successfully, create the return trip if it's round trip-details-row
                  if(model.roundTrip){  //create the return trip first if it's a round trip
                    Meteor.call('offerLift', returnLiftModel, function(error, result){
                      if(error){
                        alert("error!");
                      }
                      else if(result == true){
                        $scope.offerLiftModal.hide()
                        $state.go('Main.myLifts');
                      }
                      else{
                        popupController.showAlert("Whoa there!", "Your to-destination lift was created, but you already have a lift scheduled at the time of your return-trip lift. Deal with that conflict, then post a new return-trip lift.", function(){});
                        $state.go('Main.myLifts');
                      }
                    })
                  }
                  else{
                    $scope.offerLiftModal.hide()
                    $state.go('Main.myLifts');
                  }


                }
                else{
                  popupController.showAlert("Whoa there!", "You already have a lift scheduled for the time of your to-destination lift. Deal with that, then try again.", function(){});
                  $state.go('Main.myLifts');
                }
              })
            }
          }, "Yeah", "Nope");

        }

        else{

          Meteor.call('offerLift', model, function(error, result){
            if(error){
              alert("error!");
            }
            else if(result == true){
              // if the first lift is created successfully, create the return trip if it's round trip-details-row
              if(model.roundTrip){  //create the return trip first if it's a round trip
                Meteor.call('offerLift', returnLiftModel, function(error, result){
                  if(error){
                    alert("error!");
                  }
                  else if(result == true){
                      $scope.offerLiftModal.hide()
                      $state.go('Main.myLifts');
                  }
                  else{
                    popupController.showAlert("Whoa there!", "Your to-destination lift was created, but you already have a lift scheduled at the time of your return-trip lift. Deal with that conflict, then post a new return-trip lift.", function(){});
                    $state.go('Main.myLifts');
                  }
                })
              }
              else{
                $scope.offerLiftModal.hide()
                $state.go('Main.myLifts');
              }


            }
            else{
              popupController.showAlert("Whoa there!", "You already have a lift scheduled for the time of your to-destination lift. Deal with that, then try again.", function(){});
              $state.go('Main.myLifts');
            }
          })
        }



      }
    }
    else{
      if(model.startEndLocation == null){
        popupController.showAlert("Oh snap!", "You didn't provide a start location. Please go ahead and do that.", function(){});
      }
      else{
        popupController.showAlert("Oh snap!", "Something's not right. Please make sure you filled everything out.", function(){});
      }
    }
  };

  $scope.addSeat = function(){
    if($scope.requestOfferModel.seats < 7){
      $scope.requestOfferModel.seats++;
    }
  };
  $scope.removeSeat = function(){
    if($scope.requestOfferModel.seats > 1){
      $scope.requestOfferModel.seats--;
    }
  };

  $scope.autocompleteLocationToModel = function(autocompletePlace){
      console.log(" == autocompleteLocationToModel == ");
      console.log(autocompletePlace); //THIS IS THE AUTOCOMPLETE RETURN OBJECT

      let locationObject = {
        fullAddress: autocompletePlace.formatted_address,
        vicinity: autocompletePlace.vicinity,
        lat: autocompletePlace.geometry.location.lat(),
        long: autocompletePlace.geometry.location.lng()
      };

      $scope.requestOfferModel.startEndLocation = locationObject;
      console.log("the model startEndLocation is set to:");
      console.log($scope.requestOfferModel.startEndLocation);


  }

  $scope.hasLiftRequestsForThisTime = function(liftDepartureTime){
    let ret = false;

    //ASSUMING 60 minutes travel time for now
    var liftArrivalTime = moment(liftDepartureTime).add(60, 'minutes').toDate();
    // find all lifts user has requested
    let potentialLifts = Lifts.find({"potentialRiders.userId": Meteor.userId(), isComplete: false}).fetch();

    for(let i = 0; i < potentialLifts.length; i++){
      var iArrivalTime = moment(potentialLifts[i].departureTime).add(potentialLifts[i].estimatedTravelTime, 'minutes').toDate();

      //if a given lift overlaps with the lift that is being offered, return true.
      if(liftArrivalTime >= potentialLifts[i].departureTime && liftDepartureTime <= iArrivalTime){
        ret = true;
      }
      else{ //Otherwise return false.
        ret = false;
      }
    }
    return ret;
  }

  $scope.hasLiftForThisOfferTime = function(){

    let postedLifts = Lifts.find({"driverId": Meteor.userId(), isComplete: false, liftDirection: request.liftDirection}).fetch();

    for(var i = 0; i < postedLifts.length; i++){
      let currentLift = postedLifts[i];
      if(request.startDepartureTime < currentLift.departureTime && currentLift.departTime < request.endDepartureTime){
        return currentLift._id;
      }
    }

    return false;
  }


  uiGmapGoogleMapApi.then(function(maps) {

            var rand = 0.001 * Math.random()
            if(rand%0.0000002 == 0){
              rand = rand*-1
            }

            $scope.points = [{
                coords: {
                  latitude: request.startEndLocation.lat + rand,
                  longitude: request.startEndLocation.long + rand,
                },
                id: 1,
                title: 'Pickup Location',
                options: {
                  icon: 'PassengerPickup.png'
                }
                }]

            $scope.map = {center: {latitude: request.startEndLocation.lat + rand, longitude: request.startEndLocation.long + rand }, zoom: 10 };

            // var bounds = new google.maps.LatLngBounds();
            // for (var i = 0; i < $scope.points.length; i++) {
            // bounds.extend({lat: $scope.points[i].latitude, lng: $scope.points[i].longitude});
            // }

            // $scope.map.fitBounds(bounds);

            var styleArray = [
                    {"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},
                    {"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},
                    {"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},
                    {"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},
                    {"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},
                    {"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},
                    {"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},
                    {"featureType":"water","elementType":"all","stylers":[{"color":"#46bcec"},{"visibility":"on"}]}
                ]
            $scope.options = {
                scrollwheel: false,
                styles: styleArray,
                disableDefaultUI: true,
            };


        });


}

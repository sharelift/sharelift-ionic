angular
  .module('Sharelift')
  .controller('LoginCtrl', LoginCtrl);

function LoginCtrl ($scope, $state, $meteor, photoUploadService) {


  // $scope.testPhotoUrl = "http://graph.facebook.com/899359330145479/picture/?type=large";
  //
  // $scope.takeTestPhoto = funtion(){
  //   let newUrl = photoUploadService.uploadTestPhoto();
  //   console.log(" -- takeTestPhoto resulted in a new URL of: --");
  //   console.log(newUrl);
  //   $scope.testPhotoUrl = newUrl;
  // }

  $scope.credentials = {
    // username: "",
    // password: ""
  };
  $scope.loginErrorMessage = "";


  // --- LOGIN SCREEN ---

  $scope.fbLogin = function() {
    //Put facebook permissions in this:
    Meteor.loginWithFacebook({
        // loginStyle: "redirect",
        requestPermissions: ['email', 'user_friends', 'user_location', 'public_profile']
        }, function(err){
          if (err) {
                console.log(err);
              // throw new Meteor.Error("Facebook login failed");
          }
          else{
            console.log("Logged in!");

            Meteor.call('pullFBPicture');
            Meteor.call('pullFBFriendsData'); // pulls FB friends
            console.log("FB ID should be set.........");
            console.log(Meteor.user());

            // will execute if it's the first time logging in with Facebook
            if(!Meteor.user().profile.hasConnectedFacebook){
              Meteor.call('notifyFriendsUsingSharelift'); // send push notifications to FB friends that you're using ShareLift
              Meteor.call('setHasConnectedFacebook'); //sets the profile field hasConnectedFacebook to true so this block doesn't get executed every time the user logs in
            }

            //check if the user has a phone number:
            if(Meteor.user().profile.phone){
              console.log("User already has a phone number. Go to main");
              //redirect to Main
              $state.go('Main.ride');
              $scope.waiversCheck();
            }
            else{
              console.log("User needs to input a phone number. Go to phone input screen");
              //go to phoneInput screen
              $state.go('phoneInput');
            }



          }
      });
  }

  $scope.doLoginAction = function() { //non-facebook login

    console.log("<< doLoginAction called >>");

    $meteor.loginWithPassword($scope.credentials.username, $scope.credentials.password)
       .then(function() {
         console.log('Login success ');
         alert("logged in: " + $scope.credentials.username);
         //check if the user has a phone number:
         if(Meteor.user().profile.phone){
           console.log("User already has a phone number. Go to main");
           //redirect to Main
           $state.go('Main.ride');
           $scope.waiversCheck();
         }
         else{
           console.log("User needs to input a phone number. Go to phone input screen");
           //go to phoneInput screen
           $state.go('phoneInput');
         }

       }, function(_error) {
         console.log('Login error - ', _error);
        //  alert("Error: " + _error.reason);
         if(_error.reason == "Match failed"){
           console.log("match failed...");
           $scope.loginErrorMessage = "Your email and password seem... incompatible.";
         }
         else if(_error.reason == "User not found"){
           $scope.loginErrorMessage = "We don't have that email on file.";
         }
         else if(_error.reason == "Incorrect password"){
           $scope.loginErrorMessage = "Wrong password! Try again :)";
         }
         else{
           $scope.loginErrorMessage = "Something isn't right :/ ";
         }


        //  console.log('Login error - ', _error);
        //  if(_error.reason == "Username already exists"){
        //    $scope.createAccountError = "That username already exists";
        //  }
        //  else if(_error.reason == "Password may not be empty"){
        //    $scope.createAccountError = "You need a password!";
        //  }
        //  else{
        //    $scope.createAccountError = "Something isn't right :/ ";
        //  }
       });

     return false;
   }


$scope.goToForgotPassword = function(){
  console.log("goToForgotPassword called");
  $state.go('forgotPassword');  //navigate to the forgotPassword view
}

$scope.goToCreateAccount = function(){
  console.log("goToCreateAccount called");
  $state.go('createAccount');  //navigate to the forgotPassword view
}

$scope.goToLoginDetail = function(){
  console.log("goToLoginDetail called");
  $state.go('loginDetail');  //navigate to the forgotPassword view
}




// --- Forgot Password View ---

$scope.sendResetEmail = function(){
  let email = $scope.credentials.username;
  $scope.resetPassword(email);
}

$scope.resetPassword = function(emailAddress){
  console.log("resetPassword called");

  $meteor.forgotPassword({
    email: emailAddress
  })
  .then(function(_response) {
    console.log("Password reset email sent!");
    alert("A password reset email was sent to " + emailAddress);

  },
  function(_error) {
    console.log('Login error - ', _error);
    alert("Error: " + _error.reason);
  });

}

// --- Create Account View ---
$scope.acceptPrivacyPolicy = false;
$scope.acceptTermsConditions = false;
$scope.errorMessage = "";

$scope.toggleAcceptTermsConditions = function(){
  if($scope.acceptTermsConditions == false){
    $scope.acceptTermsConditions = true;
  }
  else{
    $scope.acceptTermsConditions = false;
  }
}

$scope.toggleAcceptPrivacyPolicy = function(){
  if($scope.acceptPrivacyPolicy == false){
    $scope.acceptPrivacyPolicy = true;
  }
  else{
    $scope.acceptPrivacyPolicy = false;
  }
}
$scope.createAccountError = "";

$scope.doCreateAccountAction = function() {
  console.log("-- CREATE ACCOUNT --");
  console.log($scope.credentials.username);
  console.log($scope.credentials.password);

  // check that both passwords match
  if($scope.credentials.password !== $scope.credentials.passwordConfirm){
    $scope.createAccountError = "The passwords you entered don't match!";
    return;
  }

  // check that the user has accepted both policies
  if($scope.acceptPrivacyPolicy == false || $scope.acceptTermsConditions == false){ // if one of the checkboxes is left unchecked:
    $scope.createAccountError = "You must accept both policies to continue using ShareLift";
    return;
  }

// ADD IN PRIVACY POLICY LOGIC AFTE ACCOUNT CREATION BELOW

  $meteor.createUser({
    username: $scope.credentials.username,
    email: $scope.credentials.username,
    password: $scope.credentials.password,
    profile: { createdOn: new Date() }
  })
  .then(function(_response) {
    console.log('doCreateAccountAction success');
    // alert("user created: " + $scope.credentials.username );

    console.log(Meteor.user());

    // update user profile to indicate they have accepted the privacy policy
    Meteor.call('acceptPrivacyPolicy', function(error, result){
      if(error){
        alert("error!");
      }
      else{
        if(result == false){  //if there is a problem, show an alert
          alert("Whoa there!", "We're not sure what's going on, but there is a problem...", function(){});
        }
        else{ //if nothing is wrong with the payment submission
          console.log(" // Privacy Policy Accepted //");
        }
      }
    });

    // update user profile to indicate they have accepted the terms & conditions
    Meteor.call('acceptTermsConditions', function(error, result){
      if(error){
        alert("error!");
      }
      else{
        if(result == false){  //if there is a problem, show an alert
          alert("Whoa there!", "We're not sure what's going on, but there is a problem...", function(){});
        }
        else{ //if nothing is wrong with the payment submission
          console.log(" // Terms & Conditions Accepted //");
        }
      }
    });


    $state.go('addFBInfo');

  },
  function(_error) {
    console.log('Login error - ', _error);
    if(_error.reason == "Username already exists"){
      $scope.createAccountError = "That username already exists";
    }
    else if(_error.reason == "Password may not be empty"){
      $scope.createAccountError = "You need a password!";
    }
    else{
      $scope.createAccountError = "Something isn't right :/ ";
    }
  });
  return false;

}


  // -- Add Facebook Info View (addFBInfo.html)
  $scope.facebookData = {
    // firstName : "",
    // lastName : "",
    // gender : "none"
  };

  // the checkbox options
  $scope.genderOptions = {
    'male': 'Male',
    'female': 'Female',
    'other': 'Other'
  };

  $scope.submitFacebookData = function(){
    console.log("-- ADD FACEBOOK ACCOUNT INFO --");
    if(!$scope.facebookData.firstName){
      alert("You need to enter a first name");
      return;
    }

    let data = {
      email : Meteor.user().username,
      firstName : $scope.facebookData.firstName,
      lastName : $scope.facebookData.lastName,
      fullName : $scope.facebookData.firstName + " " + $scope.facebookData.lastName,
      gender : $scope.facebookData.gender,
      locale : "en_US"
    }
    console.log("submitFacebookData called, with the following data:");
    console.log(data);
    Meteor.call('addFacebookInfoToUser', data);

    console.log("- Added FB data. Go to phone input -");
    $state.go('phoneInput');




  }



  $scope.waiversCheck = function(){
    //if the user hasn't signed both the Privacy Policy and the Terms & Conditions, open the waivers modal
    // let userNeedsToSign = false;
    if(Meteor.user().profile.hasAcceptedTermsConditions == true && Meteor.user().profile.hasAcceptedPrivacyPolicy == true){
      // do nothing - the user has signed both agreements
      console.log("do nothing - the user has signed both agreements");
    }
    else{
      console.log("Still needs to sign... opening modal");
      $scope.openWaiversModal(Meteor.user());
    }
  }


  // --- PHONE NUMBER input ---

  $scope.phoneModel = {
    phoneNumber : "",
    allowContact : true,
    allowContactText : "Yes",
    phoneErrorMessage : ""
  }

  $scope.toggleAllowContact = function(){
    if($scope.phoneModel.allowContact == true){
      $scope.phoneModel.allowContactText = "Yes";
    }
    if($scope.phoneModel.allowContact == false){
      $scope.phoneModel.allowContactText = "Nope";
    }
  }


  $scope.submitPhoneNumber = function(){
    console.log("-- SUBMIT PHONE --> phoneModel.phoneNumber is:");
    console.log($scope.phoneModel.phoneNumber);
    console.log($scope.phoneModel.allowContact);

    let num = String($scope.phoneModel.phoneNumber).replace(/\D/g,''); // Regex to strip out non-numerical values
    console.log(num);
    //if the number is 10 digits, insert the number:
    if(num.length == 10){
      console.log("adding number to profile");
      Meteor.call('addProfilePhoneNumber', Meteor.userId(), num);
      Meteor.call('updateCanContactUserField', $scope.phoneModel.allowContact);



      if(Meteor.user().profile.hasOnboarded == true){ //if the user has completed onboarding
        console.log(" - User has completed onboarding. Check for waiver, then go to main");
        $state.go('Main.ride');
        $scope.waiversCheck();
      }
      else{ //hasOnboarded == false
        console.log(" - User has not completed onboarding. GO to preferences");
        $state.go('inputProfileSettings1');
      }

    }
    else{
      console.log("not valid. Message is:");
      $scope.phoneModel.phoneErrorMessage = "Invalid phone number. Please provide a ten-digit number (area code plus seven digits).";
      console.log($scope.phoneModel.phoneErrorMessage);
    }

  }


  // input profile settings


  $scope.takeProfilePhoto = function(){
    $meteor.getPicture().then(function(data){
      //console.log(data);
      Meteor.call('uploadProfilePhoto', data);
    });
  }

  //gets added to the user's profile
  $scope.preferences1 = {
    // destinationId : "",
  };

  $scope.submitProfileSettings1 = function(){
    console.log("submitProfileSettings1 called");
    console.log($scope.preferences1);
    // Adds the preferences and sets “hasOnboarded” to true after
    Meteor.call('updateProfilePreferences1', $scope.preferences1);
    $state.go('inputProfileSettings2');
  }

  //gets added to the user's profile
  $scope.preferences2 = {
    // defaultRole : "",
    // defaultPickupLocation: ""
  };

  // the checkbox options for roles
  $scope.roleOptions = {
    'driver': 'drive',
    'rider': 'ride',
    'both': 'both'
  };
  $scope.defaultPickupLocationModel;

  $scope.submitProfileSettings2 = function(){
    console.log("submitProfileSettings2 called");
    console.log($scope.preferences2);
    // Adds the preferences and sets “hasOnboarded” to true after
    Meteor.call('updateProfilePreferences2', $scope.preferences2);

    if($scope.preferences2.defaultRole !== "rider"){  //the user is a driver - direct to driver signup
      console.log("Go to driver signup");
      $state.go('driverSignup');
      $scope.waiversCheck();
    }
    else{ //skip driver signup
      console.log("Skip driver sign up");
      $state.go('Main.ride');
      $scope.waiversCheck();
    }
  }

  $scope.skipProfileSettings2 = function(){
    $state.go('Main.ride');
  }

  $scope.autocompleteObjectPassSettings = function(autocompletePlace){
    // console.log(" == autocompleteObjectPass == ");
    // console.log(autocompletePlace); //THIS IS THE AUTOCOMPLETE RETURN OBJECT

    let locationObject = {
      fullAddress: autocompletePlace.formatted_address,
      vicinity: autocompletePlace.vicinity,
      lat: autocompletePlace.geometry.location.lat(),
      long: autocompletePlace.geometry.location.lng()
    };

    $scope.preferences2.defaultPickupLocation = locationObject;
    console.log(" -- Default location set to:");
    console.log($scope.preferences2.defaultPickupLocation);

}

}

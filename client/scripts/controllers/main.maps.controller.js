angular.module('Sharelift').controller('MapController', function($scope, uiGmapGoogleMapApi, $log, $timeout, $interval, $reactive){
        $reactive(this).attach($scope);
        this.helpers({
            currentLocation() {
                return Geolocation.latLng();
            }
        });

        //$scope.markers = [];


        // Geolocation.latLng(function(position) {
        //     if(position){
        //         pos = {
        //             lat: position.lat,
        //             lng: position.lng
        //         };
        //     }
        // }())

        // pos = Geolocation.latLng();

        // $scope.getLocation = function(){
        //     // console.log();
        //     return 'Worked'
        //


        //Test Comment


        $scope.getLocation = function() {
            console.log('called');
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition);
            } else {
                console.log("Geolocation has not been allowed");
            }
        }
        function showPosition(position) {
            console.log(position);
            Meteor.call("setCurrentLocation", position);

            // console.log("Latitude: " + position.coords.latitude +
            // "<br>Longitude: " + position.coords.longitude);
        }

        //$interval($scope.getLocation, 5000);



        uiGmapGoogleMapApi.then(function(maps) {
            var placeSearch, autocomplete;
            $scope.initAutocomplete = function() {
                // Create the autocomplete object, restricting the search to geographical
                // location types.
                autocomplete = new google.maps.places.Autocomplete((document.getElementById('pickupLocationInput')),{types: ['geocode']});

                // When the user selects an address from the dropdown, populate the address
                // fields in the form.
                autocomplete.addListener('place_changed', fillInAddress);
            }()

            function fillInAddress() {
                // Get the place details from the autocomplete object.
                var place = autocomplete.getPlace();
                console.log(place);

            }


            $scope.geolocate = function() {
                if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    var geolocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                    };
                    var circle = new google.maps.Circle({
                    center: geolocation,
                    radius: position.coords.accuracy
                    });
                    autocomplete.setBounds(circle.getBounds());
                });
                }
            }()

            
            $scope.map = { zoom: 9 };

            // $scope.searchbox = {
            //     template:'searchbox.tpl.html',
            //     events:{
            //         places_changed: function (searchBox) {}
            //     }
            // }
            var styleArray = [
                    {"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},
                    {"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},
                    {"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},
                    {"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},
                    {"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},
                    {"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},
                    {"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},
                    {"featureType":"water","elementType":"all","stylers":[{"color":"#46bcec"},{"visibility":"on"}]}
                ]
            $scope.options = {
                scrollwheel: false,
                styles: styleArray,
                disableDefaultUI: true,
            };
            $scope.coordsUpdates = 0;
            $scope.dynamicMoveCtr = 0;
            $scope.marker = {
            id: 0,
            coords: {
                latitude: 4,
                longitude: 4
            },
            options: { draggable: false },
            events: {
                dragend: function (marker, eventName, args) {
                $log.log('marker dragend');
                if(!pos.coords){

                }
                var lat = marker.getPosition().lat();
                var lon = marker.getPosition().lng();
                $log.log(lat);
                $log.log(lon);

                $scope.marker.options = {
                    draggable: true,
                    labelContent: "lat: " + $scope.marker.coords.latitude + ' ' + 'lon: ' + $scope.marker.coords.longitude,
                    labelAnchor: "100 0",
                    labelClass: "marker-labels"
                };
                }
            }
            };
            $scope.$watchCollection("marker.coords", function (newVal, oldVal) {
            if (_.isEqual(newVal, oldVal))
                return;
            $scope.coordsUpdates++;
            });
            $timeout(function () {
            $scope.marker.coords = {
                latitude: 42.1451,
                longitude: -100.6680
            };
            $scope.dynamicMoveCtr++;
            $timeout(function () {
                $scope.marker.coords = {
                latitude: 43.1451,
                longitude: -102.6680
                };
                $scope.dynamicMoveCtr++;
            }, 5000);
            }, 5000);


        });

    }
)

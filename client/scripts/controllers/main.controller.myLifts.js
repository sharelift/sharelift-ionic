angular.expandControllerMainMyLifts = function($scope, $reactive, $meteor, $location, popupController) {

  $scope.activeButton = 'a';

  // defines which list to show: 0 = 'Current', 1 = 'Past'
  $scope.displayList = 0;

  $scope.showCurrentLifts = function() {
    $scope.displayList = 0;
  }

  $scope.showPastLifts = function() {
    $scope.displayList = 1;
  }

  $scope.goToDetailView = function(liftId, driverId){
    console.log(" -- goToDetailView called -- ");
    console.log(liftId + " --- " + driverId);
    if(Meteor.userId() == driverId){ // if the user is the DRIVER:
      console.log("User is the driver. Going to that lift detail view");

      $location.path("index/liftDriver/" + liftId);
    }
    else{ // the user is a rider
      console.log("Directing to the standard rider lift view");
      $location.path("index/lift/" + liftId);

    }
  }

  // Routes:

  $scope.goToLiftDetailFromMyLifts = function(liftId, driverId){
    if(Meteor.userId() == driverId){ // if the user is the DRIVER:
      console.log("User is the driver. Going to that lift detail view");
      $location.path("index/myLiftDriver/" + liftId);
    }
    else{ // the user is a rider
      console.log("Directing to the standard rider lift view");
      $location.path("index/myLift/" + liftId);

    }
  }

  $scope.goToRequestDetailFromMyLifts = function(requestId){ 
      $location.path("index/myRequest/" + requestId);
  }


  $scope.goToLiftViewFromMyLifts = function(id){
    if(!Meteor.userId()){
      popupController.showAlert("Heads up!", "You aren't logged in. Please do that before viewing lifts.", function(){
        $scope.activeSlide = 0;
      });
    }
    else{
      $location.path("index/myLift/" + id);
    }
  }

    $scope.goToLiftViewDriverFromMyLifts = function(id){
      if(!Meteor.userId()){
        popupController.showAlert("Heads up!", "You aren't logged in. Please do that before viewing lifts.", function(){
          $scope.activeSlide = 0;
        });
      }
      else{
        $location.path("index/myLiftDriver/" + id);
      }
    }









}

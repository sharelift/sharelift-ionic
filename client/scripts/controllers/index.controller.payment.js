angular.expandControllerIndexPayment = function($scope, $reactive, $stateParams, $meteor, $ionicModal, $timeout, $log, $state, $controller, $location, popupController){


  $scope.submitPayment = function(paymentForm, paymentModel) {
    console.log(" -- submitPayment called --");

    // gets the first card the user has saved:
    if(Meteor.user().profile.payment){
      if(Meteor.user().profile.payment.cards[0]){
        let cardInfo = Meteor.user().profile.payment.cards[0];
        console.log(cardInfo);
      }
      else{
        console.log("The user has NO CARDS ON FILE");
      }
    }
    else{
      console.log("The user has NO CARDS ON FILE");
    }




    // cardInfo has the following fields:
        // number : ,
        // cvc : ,
        // exp_month : ,
        // exp_year : ,
        // name : "",
        // cardTitle : "",
        // dateAdded :

    // need to add a token?


// ---------

// ///////////////////////////////
//Commenting out for now (Feb. 3)

    // ccNum = $('#credit_card_number').val();
    // cvc = $('#cvv_number').val();
    // expMo = $('#month_expire').val();
    // expYr = $('#year_expire').val();
    //
    //   // Store the credit card from Rider.
    //   // It will return the object and we have to  store it
    //   Stripe.card.createToken({
    //     //number: ccNum,
    //     //cvc: cvc,
    //     //exp_month: expMo,
    //     //exp_year: expYr,
    // }, function(status, response) {
    //
    //     stripeToken = response.id;
    //     console.log('stripeToken');
    //     console.log(stripeToken);
    //     Meteor.call('chargeCard', stripeToken);
    //
    //     // For create customer with credit card information.
    //    // Meteor.call('createCustomer', stripeToken);
    //
    // });
// ///////////////////////////////

    // Store the bank account of Driver.
    // it will return the object store the bank object id
    // e.g btok_9yMQJK0k6g9Vf1
//    Stripe.bankAccount.create({
//        bank_account: {
//        country: 'US',
//        currency: 'usd',
//        account_holder_name: 'Isabella Davis',
//        account_holder_type: 'individual',
//        routing_number: '110000000',
//        account_number: '000123456789'
//  }
// }, function(err, token) {
//    console.log(token);
//});


    //check if the form is valid

    //any alerts related to what was inputted

    //submit the payment via a Meteor method call
    Meteor.call('submitPayment', paymentModel, function(error, result){
      if(error){
        alert("error!");
      }
      else{
        if(result == false){ //if there is a problem, show an alert
          popupController.showAlert("Whoa there!", "We're not sure what's going on, but there is a problem...", function(){});
        }
        else{ //if nothing is wrong with the payment submission
          console.log(" // SUBMISSION SUCCESSFUL //");
          $scope.closePaymentModal();
          $scope.activeSlide = 1;
        }
      }
    });

  };

//Modal stuff
  $ionicModal.fromTemplateUrl('client/templates/paymentScreen.html', {
    scope: $scope,
    animation: 'slide-in-up',
  }).then(function(paymentModal) {
    $scope.paymentModal = paymentModal;
  });

  $scope.openPaymentModal = function(lift) {
    if(Meteor.users.findOne({_id:Meteor.userId()})){
      $scope.thisLift = lift;

      // the model that is modified in the paymentScreen.html, and passed into submitPayment()
      $scope.paymentModel = {
        fromUserId: Meteor.userId(),
        toUserId: lift.driverId,
        liftId: lift._id,
        paymentOption: "",  //for now, this means in what way the rider decided to pay: suggested, custom, or noPay
        amount: 7
      };

      $scope.paymentModal.show();
    }
  };

  $scope.closePaymentModal = function() {
    $scope.paymentModal.hide();
  };

  //Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.paymentModal.remove();
  });

  // Execute action on hide modal
  $scope.$on('paymentModal.hidden', function() {
    // Execute action
  });

  // Execute action on remove modal
  $scope.$on('paymentModal.removed', function() {
    // Execute action
  });








}

angular
.module('Sharelift')
.controller('MainCtrl', MainCtrl);
function MainCtrl ($scope, $reactive, $meteor, $ionicModal, $state, $interval, $location, $controller, $rootScope, $cordovaCamera, findLiftInputService, liftStateService, navBarService, photoUploadService, $ionicLoading) {
  let reactiveContext = $reactive(this).attach($scope);
  var popupController = $controller('PopupCtrl', {$scope: $scope});

  //pulling in all main.controller.____ files. You can pass variables between these too, like Nathan did with popupController (see below)
  angular.expandControllerMainFixtures($scope, $reactive, $meteor, $ionicModal, $state, $interval, $location, $controller, popupController);
  angular.expandControllerMainProfile($scope, $reactive, $meteor, $ionicModal, $state, $interval, $location, $controller, popupController, $cordovaCamera, photoUploadService);
  angular.expandControllerMainLiftList($scope, $reactive, $meteor, $ionicModal, $state, $interval, $location, $controller, popupController, findLiftInputService, liftStateService);
  angular.expandControllerMainGetters($scope, $reactive, $meteor, $ionicModal, $state, $interval, $location, $controller, popupController, liftStateService);
  angular.expandControllerMainSetters($scope, $reactive, $meteor, $ionicModal, $state, $interval, $location, $controller, popupController, liftStateService);
  angular.expandControllerMainMyLifts($scope, $reactive, $meteor, $location, popupController);

  //gives access to liftStateService variables and functions within HTML, use $root.stateService._____ to access.
  $rootScope.stateService = liftStateService;

//Methods that are called once:


  //SUBS NOT DUBS=========================================================
  this.subscribe('getLocations');
  this.subscribe('getUserData');
  this.subscribe('getLiftRatings');
  this.subscribe('getRatings');
  // this.subscribe('getRiderNotificationsConfirm');
  // this.subscribe('getRiderNotificationsDeny');
  this.subscribe('getAllRiderNotifications');
  this.subscribe('getDriverNotifications');
  this.subscribe('getCancelDriverNotifications');
  // this.subscribe('getCancelRiderNotifications');
  this.subscribe('getDeletedLiftNotifications');
  this.subscribe('getPastLiftsDriver');
  this.subscribe('getPastLiftsRider');
  this.subscribe('allUserData');  //subscribes to all public data for all users - used for profile photos

  this.subscribe('liftsWhereRider');  //gets data for lifts where user is a rider
  this.subscribe('liftsWhereDriver'); //gets data for lifts where user is a driver
  this.subscribe('getOpenRequests'); //gets open requests

  reactiveContext.helpers({

    getLoggedInUser: function() {
      $scope.updatePhoneInput();
      $scope.updatePickupInput();
      $scope.updateVehicleInput();
      console.log(Meteor.user());
    }
  });

  this.helpers({
    openRequests(){
      return RideRequests.find({isAccepted : false, endDepartureTime: { $gt: new Date()}})
    },
    openLifts() { //gets all lifts
      return Lifts.find({isComplete : false, departureTime: { $gt: new Date()}});
    },
    confirmedLifts() {  //gets all lifts where the user is a driver or confirmed rider
      return Lifts.find({ $or: [{isComplete : false, confirmedRiders : {$elemMatch: {userId: Meteor.userId()}}}, {isComplete : false, driverId : Meteor.userId()}]});
    },
    // riderLifts() {  //gets all lifts where the user is a confirmed rider
      // return Lifts.find({isComplete : false, confirmedRiders : {$elemMatch: {userId: Meteor.userId()}}});
    // },
    // driverLifts() { //gets all lifts where the user is the driver
      // return Lifts.find({isComplete : false, driverId : Meteor.userId()});
    // },

    myCurrentLifts() {  //gets all lifts that belong to the current user (as a rider or driver)
      // var currentLiftCutoff = moment(new Date()).subtract(12, 'hours').toDate();

      return Lifts.find({ isComplete : false, $or: [ { confirmedRiders : {$elemMatch: {userId: Meteor.userId()}} }, { driverId : Meteor.userId() } ] });
    },

    myCurrentRequests(){
      return RideRequests.find({riderId: Meteor.userId(), endDepartureTime: { $gt: new Date()}})
    },
    // myPastLifts() {  //gets all lifts that belong to the current user (as a rider or driver)
    //   // var currentLiftCutoff = moment(new Date()).subtract(12, 'hours').toDate();
    //
    //   console.log("THE CUTOFF TIME");
    //   console.log(cutoffTime);
    //   return Lifts.find({ $or: [{ isComplete : true }], $or: [ { confirmedRiders : {$elemMatch: {userId: Meteor.userId()}} }, { driverId : Meteor.userId() } ] });
    // },
// , { departureTime: {$lt: currentLiftCutoff} }

    myPastLifts() {  //gets all lifts that belong to the current user (as a rider or driver)
      return Lifts.find({ isComplete : true, $or: [ { confirmedRiders : {$elemMatch: {userId: Meteor.userId()}} }, { driverId : Meteor.userId() } ] });
    },
    user() {
      return Meteor.user();
    },
    notificationListView() {
      return Notifications.find({dismissed:false});
    },
    completedLiftsRider() {
      return Lifts.find({confirmedRiders : {$elemMatch: {userId: Meteor.userId()}}, isComplete : true});
    },
    completedLiftsDriver() {
      return Lifts.find({isComplete : true, driverId : Meteor.userId()});
    },

    destinations() {
      return Locations.find({destination: true});
    },

  });



  $scope.activeSlide = 1;
  $scope.isIndex = 'true';
  $scope.navTitle='<img class="title-image" src="img/sharelift_elementsV1_4.svg" width="100"/>';
  // $scope.locations = [];

  $scope.offerLiftAddress;



  $scope.popupClosed = function(value){
    //console.log(value);
  }
  $scope.alertDismissed = function(){
    //Do things here.
  }

  $scope.printOut = function(input){
    console.log(input);
  }

  // $scope.slideHasChanged = function($index){  //use this to perform actions upon slide change
  //   // For example, display a popup:
  //   console.log(" <<<<< the slide changed! >>>>>>");
  //   console.log($index);
  //   navBarService.currentSlide = $index;
  //   console.log("navBarService.currentSlide now equal to:");
  //   console.log(navBarService.currentSlide);
  //
  //   //var buttons = [ {text:'Cancel'}, { text: 'Okay', onTap: $scope.testFunction } ];
  //   //var buttons = [ {text:'Cancel'}, { text: 'Okay'} ];
  //   //var myPopup = popupController.showPopup("This is a popup", "It has a subtitle", "<input type='text' ng-model='inputValue'/>", buttons);
  //   //popupController.showConfirm("Oh snap!", "this is the body.", $scope.popupClosed);
  //   //popupController.showAlert("Dang it!", "This is just an alert. Carry on.", $scope.alertDismissed);
  //   //popupController.showInputPopup("Oh snap!", "Please provide your phone number first!", function(val){console.log(val)});
  // };


  // $rootScope.$on("goToSlide", function(event, data){
  //   $scope.activeSlide = data;
  // })


// Notifications / Alerts stuff ---

  $scope.routeToLift = function(notif){
    // get the right URL

    var url;
    if(notif.driverId == Meteor.userId()){ //user is the driver
      // url = "#/driverLift/" + notif.liftId;
      $location.path("index/myLiftDriver/" + notif.liftId);
      console.log("Navigating to: " + "index/driverLift/" + notif.liftId);
      // return url;
    } else if(notif.notifType == 'sendOffer'){
      $location.path("index/notifRequest/" + notif.requestId)
    }
    else{ //user is not the driver
      // url = "index/lift/" + notif.liftId
      $location.path("index/myLift/" + notif.liftId);


      console.log("Navigating to: " + "index/myLift/" + notif.liftId);
      // return url;
    }

    $scope.markNotifRead(notif);


  }





  ////////////////////////////////////////////////////////
  //                   APP STATES
  ///////////////////////////////////////////////////////

  $scope.isLoggedIn = function() { //returns true if user is logged in
    if(Meteor.user()) {
      //console.log("isLoggedIn:true");
      return true;
    }
    //console.log("isLoggedIn:false")
    return false;
  };


  // $scope.isDriverInThisLift = function(lift){
  //   if(Meteor.userId() == lift.driverId){
  //     return true;
  //   }
  //   else{
  //     return false;
  //   }
  // }



// change this so that you have to pass in a lift --> ratingNeeded(lift), which makes it applicable to any lift
  $scope.ratingNeeded = function() {
    //returns true if the user still needs to rate & pay
    if(Meteor.user()){
      // ------ ADD LOGIC HERE -------
      // DO A CHECK TO SEE IF THE USER IS A RIDER OR DRIVER IN A LIFT THAT
    }
    //for right now, this is always returning false
    return false;
  };



  ////////////////////////////////////////////////////////
  //                   FACEBOOK STUFF
  ///////////////////////////////////////////////////////

  // $scope.fbLogin = function() {
  //   //Put facebook permissions in this:
  //   Meteor.loginWithFacebook({
  //       // loginStyle: "redirect",
  //       requestPermissions: ['email', 'user_friends', 'user_location', 'public_profile']
  //       }, function(err){
  //         if (err) {
  //               console.log(err);
  //             // throw new Meteor.Error("Facebook login failed");
  //         }
  //         else{
  //           console.log("Logged in!");
  //
  //           Meteor.call('pullFBPicture');
  //           Meteor.call('pullFBFriendsData');
  //           console.log("FB ID should be set.........");
  //           console.log(Meteor.user());
  //
  //           Meteor.call('notifyFriendsUsingSharelift');
  //
  //
  //           //if the user hasn't signed both the Privacy Policy and the Terms & Conditions, open the waivers modal
  //           // let userNeedsToSign = false;
  //           if(Meteor.user().profile.hasAcceptedTermsConditions == true && Meteor.user().profile.hasAcceptedPrivacyPolicy == true){
  //             // do nothing - the user has signed both agreements
  //             console.log("do nothing - the user has signed both agreements");
  //           }
  //           else{
  //             console.log("Still needs to sign... opening modal");
  //             $scope.openWaiversModal(Meteor.user());
  //           }
  //
  //         }
  //     });
  // }

  $scope.testWavierAction = function() {
    console.log("Waiver reset");
    Meteor.call('resetWaivers');
  }

  $scope.goToProfilePayment = function(){
    console.log(" -- goToProfilePayment called. Try going to login --");
    $state.go('profilePayment');
  }

  $scope.fbLogout = function() {
    popupController.showConfirm("Heads up!", "Are you sure you want to log out?", function(confirm){
      if(confirm){
      // old logout function
        // Meteor.logout(function(err){
        //   if (err) {
        //     throw new Meteor.Error("Logout failed");
        //   }
        // });

        $meteor.logout().then(function(_response) {
          // close the profile modal
          $rootScope.closeProfileModal();
          // go to the login screen
          $state.go('login');
        });

        // // go the login page
        // $state.go('login');

      }
    }, "Positive", "Nevermind");
  }



  //Reactive helpers. Must return a meteor collection. I think. Maybe.


  $ionicModal.fromTemplateUrl('client/templates/createLift.html', {
    scope: $scope,
    animation: 'slide-in-up',
  }).then(function(modal) {
    $scope.offerModal = modal;
  });

  $ionicModal.fromTemplateUrl('client/templates/createRequest.html', {
    scope: $scope,
    animation: 'slide-in-up',
  }).then(function(modal) {
    $scope.requestModal = modal;
  });

  $scope.openOfferLiftModal = function() {
    if(Meteor.users.findOne({_id:Meteor.userId()})){
      let user = Meteor.users.findOne({_id:Meteor.userId()});
      if(!user.profile.vehicle.make || !user.profile.vehicle.model || !user.profile.vehicle.year){
        popupController.showAlert("Oh snap!", "Looks like you didn't provide your vehicle info! Please go do that first.", function(){
          $state.go('Main.profile');
        });
      }
      else if(!user.profile.phone){
        popupController.showAlert("Oh snap!", "Looks like you didn't provide a phone number! Please go do that first.", function(){
          $state.go('Main.profile');
        });
      }
      else{
        $scope.offerLiftModel = {  //location, dest, date, depart, return, seats
          liftDirection: "toDestination",
          startEndLocation: $rootScope.currentUser.profile.settings.defaultPickupLocation || null,
          endLocation: '',
          destinationId: null,
          departureTime: null,
          returnTime: "",
          seats: 2,
          roundTrip: false,
          offerId: null
        };
        $scope.offerModal.show();
      }
      console.log(user);
    }
  };

  $scope.openCreateRequestModal = function() {
    if(Meteor.users.findOne({_id:Meteor.userId()})){
      let user = Meteor.users.findOne({_id:Meteor.userId()});
      if(!user.profile.phone){
        popupController.showAlert("Oh snap!", "Looks like you didn't provide a phone number! Please go do that first.", function(){
          $state.go('Main.profile');
        });
      } else {
        $scope.createRequestModel = {  //location, dest, date, depart
          liftDirection: "toDestination",
          startEndLocation: $rootScope.currentUser.profile.settings.defaultPickupLocation || null,
          endLocation: '',
          destinationId: null,
          startDepartureTime: "",
          endDepartureTime: "",
        };
        $scope.requestModal.show();
      }
      console.log(user);
    }
  };

  $scope.closeOfferLiftModal = function() {
    $scope.offerModal.hide();
  };

  $scope.closeCreateRequestModal = function() {
    $scope.requestModal.hide();
  };

  //Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.requestModal.remove();
    $scope.offerModal.remove();
  });

  // Execute action on hide modal
  $scope.$on('modal.hidden', function() {
    // Execute action
  });

  // Execute action on remove modal
  $scope.$on('modal.removed', function() {
    // Execute action
  });

  $scope.timePickerDeparture = {
    inputEpochTime: 32400,  //Optional
    step: 15,  //Optional
    format: 12,  //Optional
    titleLabel: '12-hour Format',  //Optional
    setLabel: 'Set',  //Optional
    closeLabel: 'Close',  //Optional
    setButtonType: 'button-positive',  //Optional
    closeButtonType: 'button-stable',  //Optional
    callback: function (val) {    //Mandatory
      if(val) {
        this.inputEpochTime = val;
        var t = new Date(val * 1000);
        console.log(t);
        $scope.offerLiftModel.departureTime = t;
      }
    }
  };

  // $scope.timePickerReturn = {
  //   inputEpochTime: 57600,  //Optional
  //   step: 15,  //Optional
  //   format: 12,  //Optional
  //   titleLabel: '12-hour Format',  //Optional
  //   setLabel: 'Set',  //Optional
  //   closeLabel: 'Close',  //Optional
  //   setButtonType: 'button-positive',  //Optional
  //   closeButtonType: 'button-stable',  //Optional
  //   callback: function (val) {    //Mandatory
  //     if(val) {
  //       this.inputEpochTime = val;
  //       var t = new Date(val * 1000);
  //       console.log(t);
  //       $scope.offerLiftModel.returnTime = t;
  //     }
  //   }
  // };

  function timePickerCallback(val) {
    if (typeof (val) === 'undefined') {
      console.log('Time not selected');
    } else {
      var selectedTime = new Date(val * 1000);
      console.log('Selected epoch is : ', val, 'and the time is ', selectedTime.getUTCHours(), ':', selectedTime.getUTCMinutes(), 'in UTC');
    }
  };

  $scope.offerLiftModel = {  //location, dest, date, depart, return, seats
    liftDirection : "toDestination",
    startEndLocation : $rootScope.currentUser.profile.settings.defaultPickupLocation || null,
    endLocation : '',
    destinationId : null,
    departureTime : null,
    returnTime : "",
    seats : 2,
    roundTrip : false,
    offerId: null
  };

  $scope.offerResort;

  $scope.hasSelectedOfferResort = function(){
    if($scope.selectedResort){
      return true;
    }
    else{
      return false;
    }
  }

  /// Select Destination Modal ///

  $ionicModal.fromTemplateUrl('client/templates/selectOfferDestination.html', {
    scope: $scope,
    animation: 'slide-in-up',
  }).then(function(modal3) {
    $scope.offerDestinationModal = modal3;
  });

    $scope.openOfferDestinationModal = function() {
        $scope.offerDestinationModal.show();
    };

    $scope.closeOfferDestinationModal = function() {
      $scope.offerDestinationModal.hide();
    };

    //Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function() {
      $scope.offerDestinationModal.remove();
    });

    // Execute action on hide modal
    $scope.$on('offerDestinationModal.hidden', function() {
      // Execute action
    });

    // Execute action on remove modal
    $scope.$on('offerDestinationModal.removed', function() {
      // Execute action
    });

    $scope.setOfferDestination = function(locationObject){
      console.log(locationObject);
      //Set the resortValue property to the selected location ID
      $scope.offerResort = locationObject;
      $scope.offerLiftModel.destinationId = locationObject.locationId;

      //close the modal
      $scope.closeOfferDestinationModal();
    };

  $scope.timeChanged = function () {
    $log.log('Time changed to: ' + $scope.mytime);
  };

  $scope.offerLift = function(form, model) {
    console.log("-- offerLift called --");

    if(form.$valid === true) {
      var currentTime = new Date();
      console.log(model);
      console.log(currentTime);

      if(!Meteor.user().profile.vehicle.make || !Meteor.user().profile.vehicle.model || !Meteor.user().profile.vehicle.year){
        popupController.showAlert("Oh snap!", "You're missing some vehicle info. You'll need to add it before creating a lift.", function(){});
        $state.go('Main.profile');
      }
      else if(model.departureTime == null){
        popupController.showAlert("Oh snap!", "Select a departure time", function(){});
      }
      else if(!model.destinationId){
        popupController.showAlert("Oh snap!", "You gotta provide a destination, son!", function(){});
      }
      else if(model.roundTrip && model.returnTime < model.departureTime){
          popupController.showAlert("Oh snap!", "Your return time must be after your departure time.", function(){});
      }
      // .add is not a function you can use on a regular Date object (only for Moment objects), so need to change this
      else if(moment(model.departureTime) <= moment(currentTime).add(0, 'minutes').toDate()){
        popupController.showAlert("Oh snap!", "You can't post a lift that departs less than 10 minutes into the future.", function(){});
      }
      else if(!model.destinationId){
        popupController.showAlert("Oh snap!", "You gotta provide a destination, son!", function(){});
      }
      else{

        //Check if the user has requests for lifts at the same time
        let isConflict = $scope.hasLiftRequestsForThisTime(model.departureTime);
        let isReturnTripConflict = false;
        let returnLiftModel;

        //if this is a round-trip lift:
        if(model.roundTrip){
          isReturnTripConflict = $scope.hasLiftRequestsForThisTime(model.returnTime);

          //create a new model for the return lift:
          returnLiftModel = {
            liftDirection : "fromDestination",
            startEndLocation : model.startEndLocation,
            endLocation : '',
            destinationId : model.destinationId,
            departureTime : model.returnTime,
            returnTime : "",
            seats : model.seats,
            roundTrip : true,
            offerId: null
          };
        }

        //if there is a conflict on the to-trip
        if(isConflict || isReturnTripConflict){
          popupController.showConfirm("Heads up!", "Posting a lift will cancel any lift requests you've sent to other drivers for the same time. Is that okay?", function(confirm){
            if(confirm){

              // show the loading popup until the state is changed
              $ionicLoading.show({
                  template: 'Creating lift...',
                  // template: <ion-spinner icon="ripple" class="spinner-assertive"></ion-spinner>,
                  hideOnStateChange: true
                });

              Meteor.call('offerLift', model, function(error, result){
                if(error){
                  alert("error! - 1st offer lift call for lift conflict");
                }
                else if(result == true){
                  // if the first lift is created successfully, create the return trip if it's round trip-details-row
                  if(model.roundTrip){  //create the return trip first if it's a round trip
                    Meteor.call('offerLift', returnLiftModel, function(error, result){
                      if(error){
                        alert("error! - second offerLift call for lift conflict");
                      }
                      else if(result == true){
                        $scope.closeOfferLiftModal();
                        
                        $scope.offerLiftModel = {  //location, dest, date, depart, return, seats
                          liftDirection: "toDestination",
                          startEndLocation: $rootScope.currentUser.profile.settings.defaultPickupLocation || null,
                          endLocation: '',
                          destinationId: null,
                          departureTime: null,
                          returnTime: "",
                          seats: 2,
                          roundTrip: false,
                          offerId: null
                        };

                        $state.go('Main.myLifts');
                      }
                      else{
                        $ionicLoading.hide();
                        popupController.showAlert("Whoa there!", "Your to-destination lift was created, but you already have a lift scheduled at the time of your return-trip lift. Deal with that conflict, then post a new return-trip lift.", function(){});
                        $scope.closeOfferLiftModal();
                        $state.go('Main.myLifts');
                      }
                    })
                  }
                  else{
                    $scope.closeOfferLiftModal();

                    $scope.offerLiftModel = {  //location, dest, date, depart, return, seats
                          liftDirection: "toDestination",
                          startEndLocation: $rootScope.currentUser.profile.settings.defaultPickupLocation || null,
                          endLocation: '',
                          destinationId: null,
                          departureTime: null,
                          returnTime: "",
                          seats: 2,
                          roundTrip: false,
                          offerId: null
                        };

                    $state.go('Main.myLifts');
                  }


                }
                else{
                  $ionicLoading.hide();
                  popupController.showAlert("Whoa there!", "You already have a lift scheduled for the time of your to-destination lift. Deal with that, then try again.", function(){});
                  $scope.closeOfferLiftModal();
                  $state.go('Main.myLifts');
                }
              })
            }
          }, "Yeah", "Nope");

        }

        else{

          // show the loading popup until the state is changed
          $ionicLoading.show({
              template: 'Creating lift...',
              // template: <ion-spinner icon="ripple" class="spinner-assertive"></ion-spinner>,
              hideOnStateChange: true
            });

          Meteor.call('offerLift', model, function(error, result){
            if(error){
              alert("error! - 1st offer lift call");
              console.log("The error is:");
              console.log(error);
            }
            else if(result == true){
              // if the first lift is created successfully, create the return trip if it's round trip-details-row
              if(model.roundTrip){  //create the return trip first if it's a round trip
                Meteor.call('offerLift', returnLiftModel, function(error, result){
                  if(error){
                    alert("error! - 2snd offer lift call");
                  }
                  else if(result == true){
                    $scope.closeOfferLiftModal();

                    $scope.offerLiftModel = {  //location, dest, date, depart, return, seats
                          liftDirection: "toDestination",
                          startEndLocation: $rootScope.currentUser.profile.settings.defaultPickupLocation || null,
                          endLocation: '',
                          destinationId: null,
                          departureTime: null,
                          returnTime: "",
                          seats: 2,
                          roundTrip: false,
                          offerId: null
                        };

                    $state.go('Main.myLifts');
                  }
                  else{
                    $ionicLoading.hide();
                    popupController.showAlert("Whoa there!", "Your to-destination lift was created, but you already have a lift scheduled at the time of your return-trip lift. Deal with that conflict, then post a new return-trip lift.", function(){});
                    $scope.closeOfferLiftModal();
                    $state.go('Main.myLifts');
                  }
                })
              }
              else{
                $scope.closeOfferLiftModal();

                $scope.offerLiftModel = {  //location, dest, date, depart, return, seats
                          liftDirection: "toDestination",
                          startEndLocation: $rootScope.currentUser.profile.settings.defaultPickupLocation || null,
                          endLocation: '',
                          destinationId: null,
                          departureTime: null,
                          returnTime: "",
                          seats: 2,
                          roundTrip: false,
                          offerId: null
                        };

                $state.go('Main.myLifts');
              }


            }
            else{
              $ionicLoading.hide();
              popupController.showAlert("Whoa there!", "You already have a lift scheduled for the time of your to-destination lift. Deal with that, then try again.", function(){});
              $scope.closeOfferLiftModal();
              $state.go('Main.myLifts');
            }
          })
        }

      }
    }
    else{
      if(model.startEndLocation == null){
        popupController.showAlert("Oh snap!", "You didn't provide a start location. Please go ahead and do that.", function(){});
      }
      else{
        popupController.showAlert("Oh snap!", "Something's not right. Please make sure you filled everything out.", function(){});
      }
    }
  };


  $scope.createRequestModel = {  //location, dest, date, depart
    liftDirection : "toDestination",
    startEndLocation : $rootScope.currentUser.profile.settings.defaultPickupLocation || null,
    endLocation : '',
    destinationId : null,
    startDepartureTime : "",
    endDepartureTime : "",
  };


  $scope.createRequest = function(form, model) {
    //let returnTimeFake = moment(model.departureTime).add(8, 'hours').toDate();

    //Only have this if you want the returnTime = departureTime - for liability purposes
    //model.returnTime = model.departureTime;
    console.log("-- createRequest called --");


    if(form.$valid === true) {
      var currentTime = new Date();
      console.log(model);
      console.log(currentTime);

      if(!model.startDepartureTime){
        popupController.showAlert("Oh snap!", "Select a start time for your departure time slot", function(){});

      } else if(!model.endDepartureTime){
        popupController.showAlert("Oh snap!", "Select an end time for your departure time slot", function(){});

      } else if(!model.destinationId){
        popupController.showAlert("Oh snap!", "You gotta provide a destination, son!", function(){});

      } else if(!model.destinationId){
        popupController.showAlert("Oh snap!", "You gotta provide a destination, son!", function(){});

      } else if(!model.startEndLocation && model.liftDirection == 'toDestination'){
        popupController.showAlert("Oh snap!", "You gotta provide a pickup location, son!", function(){});

      } else if(!model.startEndLocation && model.liftDirection == 'fromDestination'){
        popupController.showAlert("Oh snap!", "You gotta provide a dropoff location, son!", function(){});

      } else if(model.startDepartureTime < currentTime || model.endDepartureTime < currentTime){
        popupController.showAlert("Oh snap!", "You can't request a lift that departs in the past. Unless you're Doc Brown, but we both know you aren't.", function(){});
      }
      else if(model.startDepartureTime >= model.endDepartureTime){
        popupController.showAlert("Oh snap!", "Your start time can't be past your end time", function(){});
      }
      // .add is not a function you can use on a regular Date object (only for Moment objects), so need to change this
      // else if(moment(model.endDepartureTime) <= moment(currentTime).add(10, 'minutes').toDate()){
      //   popupController.showAlert("Oh snap!", "You can't post a lift that departs less than 10 minutes into the future.", function(){});
      // }
      else if(!model.destinationId){
        popupController.showAlert("Oh snap!", "You gotta provide a destination, son!", function(){});
      }
      else if(!model.startDepartureTime || !model.endDepartureTime){
        popupController.showAlert("Oh snap!", "You gotta provide a pick up time zone, son!", function(){});
      }
      else{

        //Check if the user has requests for lifts at the same time
        let isConflict = $scope.hasLiftRequestsForThisTimeSlot(model.startDepartureTime, model.endDepartureTime);

        //if there is a conflict on the to-trip
        if(isConflict){

          //Will look for other lift requests during this time

          popupController.showConfirm("Heads up!", "Requesting a lift for these times will cancel any lift requests you've posted for the same time. Is that okay?", function(confirm){
            if(confirm){

              Meteor.call('createRequest', model, function(error, result){
                if(error){
                  alert("error!");
                }
                else if(result == true){
                  $scope.requestModal.hide();

                    $scope.createRequestModel = {  //location, dest, date, depart
                      liftDirection: "toDestination",
                      startEndLocation: $rootScope.currentUser.profile.settings.defaultPickupLocation || null,
                      endLocation: '',
                      destinationId: null,
                      startDepartureTime: "",
                      endDepartureTime: "",
                    };

                    $state.go('Main.myLifts');

                }
                else{
                  popupController.showAlert("Whoa there!", "You already have a lift scheduled within the time of your ride request. Deal with that, then try again.", function(){});
                  $state.go('Main.myLifts');
                }
              })
            }
          }, "Yeah", "Nope");

        } else {

          Meteor.call('createRequest', model, function(error, result){
                if(error){
                  console.log(error)
                  alert("error!");
                }
                else if(result == true){
                    console.log('State Change here')
                    $scope.requestModal.hide();

                    $scope.createRequestModel = {  //location, dest, date, depart
                      liftDirection: "toDestination",
                      startEndLocation: $rootScope.currentUser.profile.settings.defaultPickupLocation || null,
                      endLocation: '',
                      destinationId: null,
                      startDepartureTime: "",
                      endDepartureTime: "",
                    };
                    
                    $state.go('Main.myLifts');

                }
                else{
                  popupController.showAlert("Whoa there!", "You already have a lift scheduled within the time of your ride request. Deal with that, then try again.", function(){});
                  $state.go('Main.myLifts');
                }
              })
        }


      }
    }
    else{
      if(model.startEndLocation == null){
        popupController.showAlert("Oh snap!", "You didn't provide a start location. Please go ahead and do that.", function(){});
      }
      else{
        popupController.showAlert("Oh snap!", "Something's not right. Please make sure you filled everything out.", function(){});
      }
    }
  };


  $scope.addSeat = function(){
    if(this.offerLiftModel.seats < 7){
      this.offerLiftModel.seats = parseInt(this.offerLiftModel.seats) + 1;
    }
  };
  $scope.removeSeat = function(){
    if(this.offerLiftModel.seats > 1){
      this.offerLiftModel.seats = parseInt(this.offerLiftModel.seats) - 1;
    }
  };

  $scope.toggleLiftDirection = function(){
    if(this.offerLiftModel.liftDirection == 'toDestination' || $scope.createRequestModel.liftDirection == 'toDestination'){
      this.offerLiftModel.liftDirection = 'fromDestination';
      $scope.createRequestModel.liftDirection = 'fromDestination';
    }
    else{
      this.offerLiftModel.liftDirection = 'toDestination';
      $scope.createRequestModel.liftDirection = 'toDestination';
    }
  };

  $scope.autocompleteLocationToModel = function(autocompletePlace){
      console.log(" == autocompleteLocationToModel == ");
      console.log(autocompletePlace); //THIS IS THE AUTOCOMPLETE RETURN OBJECT

      let locationObject = {
        fullAddress: autocompletePlace.formatted_address,
        vicinity: autocompletePlace.vicinity,
        lat: autocompletePlace.geometry.location.lat(),
        long: autocompletePlace.geometry.location.lng()
      };

      $scope.offerLiftModel.startEndLocation = locationObject;
      $scope.createRequestModel.startEndLocation = locationObject;
      console.log("the model startEndLocation is set to:");
      console.log($scope.offerLiftModel.startEndLocation);


  }

  $scope.getMatrixTimeDistance = function(){
    //get the start location:
    var start = {
      lat: 45.6794836,
      long: -111.0352398,
    };

    //get the end location
    let destination = Locations.findOne({locationId: 3});
    var end = {
      lat: destination.latitude,
      long: destination.longitude,
    };

    //calls a synchronous http request on the server
    Meteor.call('getMatrixTimeDistance', start, end, function(error, routeInfo){
      if(error){
        alert("error!");
      }
      else{
        console.log(" -- getMatrixTimeDistance called. The result is: --");
        console.log(routeInfo);
        //do something with it...
      }
    });
  };


  $scope.getLiftDay = function(lift){
    var weekday = new Array(7);
    weekday[0]=  "Sunday";
    weekday[1] = "Monday";
    weekday[2] = "Tuesday";
    weekday[3] = "Wednesday";
    weekday[4] = "Thursday";
    weekday[5] = "Friday";
    weekday[6] = "Saturday";
    let ret;
    let liftDate;
    if(lift && lift.date){
      if(lift.date._d){
          liftDate = new Date(lift.date._d);
      }
      else{
          liftDate = new Date(lift.date);
      }
      if(liftDate && liftDate.getFullYear()){
        let date1 = new Date();
        var date1_tomorrow = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate() + 1);
        if(date1.getFullYear() == liftDate.getFullYear() && date1.getMonth() == liftDate.getMonth() && date1.getDate() == liftDate.getDate()){
          ret = "Today";
        }
        else if (date1_tomorrow.getFullYear() == liftDate.getFullYear() && date1_tomorrow.getMonth() == liftDate.getMonth() && date1_tomorrow.getDate() == liftDate.getDate()) {
          ret = "Tomorrow"; // date2 is one day after date1.
        }
        else{
          ret = String(weekday[liftDate.getDay()]);
        }
      }
    }
    return(ret);
  }

  $scope.getLiftMonthDay = function(lift){
    var month = new Array(12);
    month[0]=  "January";
    month[1] = "February";
    month[2] = "March";
    month[3] = "April";
    month[4] = "May";
    month[5] = "June";
    month[6] = "July";
    month[7] = "August";
    month[8] = "September";
    month[9] = "October";
    month[10] = "November";
    month[11] = "December";
    let ret;
    let liftDate;
    if(lift && lift.date){
      if(lift.date._d){
          liftDate = new Date(lift.date._d);
      }
      else{
          liftDate = new Date(lift.date);
      }
      ret = month[liftDate.getMonth()] + " " + liftDate.getDate();
    }
    return ret;
  }


}

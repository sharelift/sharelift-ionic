angular.expandControllerMainFixtures = function($scope, $reactive, $meteor, $ionicModal, $state, $interval, $location, $controller, popupController){

  $scope.groups = [];

  $scope.groups[0] = {
    name:"Secret buttons, for smart people only",
    items:[],
    show:false
  }
  $scope.groups[0].items.push({function:"insertFixtureNotifications()", name:"Insert notifications"});
  $scope.groups[0].items.push({function:"insertFixtureOpenLiftDriver()", name:"Insert open lift, driver"});
  $scope.groups[0].items.push({function:"insertFixtureOpenLiftRider()", name:"Insert open lift, rider"});
  $scope.groups[0].items.push({function:"insertFixtureOpenLiftDriverFarOffDude()", name:"Insert distant lift, driver"});
  $scope.groups[0].items.push({function:"insertFixtureOpenLiftRiderFarOffDude()", name:"Insert distant lift, rider"});
  $scope.groups[0].items.push({function:"insertFixtureCompletedLiftDriver()", name:"Insert completed lift, driver"});
  $scope.groups[0].items.push({function:"insertFixtureCompletedLiftRider()", name:"Insert completed lift, rider"});
  $scope.groups[0].items.push({function:"insertFixtureCompletedLiftDriverRequireRatings()", name:"Insert completed lift, driver, ratings req."});
  $scope.groups[0].items.push({function:"insertFixtureDriverRatings()", name:"Insert driver ratings"});
  $scope.groups[0].items.push({function:"printFBFriendsDataToConsole()", name:"Print FB data to console"});
  $scope.groups[0].items.push({function:"insertFixtureLifts()", name:"Insert fixture lifts"});
  $scope.groups[0].items.push({function:"testPushNotifs()", name:"Send push notification to all friends"});
  $scope.groups[0].items.push({function:"resetData()", name:"Reset the database ಠ_ಠ"});


  /*
   * if given group is the selected group, deselect it
   * else, select the given group
   */
  $scope.toggleGroup = function(group) {
    group.show = !group.show;
  };
  $scope.isGroupShown = function(group) {
    return group.show;
  };

  // End collapse stuff


  $scope.insertFixtureNotifications = function() {
    console.log("fuck");
    Meteor.call('insertFixtureNotifications');
  }

  $scope.insertFixtureOpenLiftDriver = function() {
    console.log("angularjs is balls");
    Meteor.call('insertFixtureOpenLiftDriver');
  }

  $scope.insertFixtureOpenLiftRider = function() {
    console.log("why");
    Meteor.call('insertFixtureOpenLiftRider');
  }

  $scope.insertFixtureOpenLiftRiderFarOffDude = function() {
    console.log("freak a leak");
    Meteor.call('insertFixtureOpenLiftRiderFarOffDude');
  }

  $scope.insertFixtureOpenLiftDriverFarOffDude = function() {
    console.log("mothafuckin we callin");
    Meteor.call('insertFixtureOpenLiftDriverFarOffDude');
  }

  $scope.insertFixtureCompletedLiftDriver = function() {
    console.log("gettin on that grind");
    Meteor.call('insertFixtureCompletedLiftDriver');
  }

  $scope.insertFixtureCompletedLiftRider = function() {
    console.log("yurp");
    Meteor.call('insertFixtureCompletedLiftRider');
  }

  $scope.insertFixtureCompletedLiftDriverRequireRatings = function() {
    Meteor.call('insertFixtureCompletedLiftDriverRequireRatings');
  }

  $scope.insertFixtureDriverRatings = function(){
    Meteor.call('insertFixtureDriverRatings');
  }

  $scope.printFBFriendsDataToConsole = function() {
    console.log("printFBFriendsDataToConsole() called. These are your friends: ");
    console.log(Meteor.user().profile.facebookFriends);
  }

  $scope.insertFixtureLifts = function(){
    Meteor.call('insertFixtureLifts');
  }

  $scope.testPushNotifs = function() {
    Meteor.call('testPushNotifications');
  };

  $scope.resetData = function() {
    Meteor.call('resetData');
  };


}

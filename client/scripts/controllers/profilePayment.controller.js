angular
  .module('Sharelift')
  .controller('ProfilePaymentCtrl', ProfilePaymentCtrl);

function ProfilePaymentCtrl ($scope, $state, $meteor, $ionicModal, $reactive) {

  let reactiveContext = $reactive(this).attach($scope);


  $scope.accountBalance = 30.50;

  // Modal hiddne & remove operations (for all modals)
  $scope.$on('modal.hidden', function() { // Execute action on hide modal
    // Execute action
  });
  $scope.$on('modal.removed', function() { // Execute action on remove modal
    // Execute action
  });



  this.helpers({
    myCards() { //gets all user's cards
      if(Meteor.user().profile.payment.cards){
        return Meteor.user().profile.payment.cards;
      }
      else{
        let emptyArray = [];
        return emptyArray;
      }
    },
    myBanks() { //gets all user's connected banks
      if(Meteor.user().profile.payment.banks){
        return Meteor.user().profile.payment.banks;
      }
      else{
        let emptyArray = [];
        return emptyArray;
      }
    }
  });


  // TRANSFER MONEY -----------------------------------------------------------

  $scope.transferMoney = function(){

    // add transfer logic here
    // ...
    // ...
    // ...
    // ...

    alert("money transfered to your bank!");
    $scope.closeTransferMoneyModal(); //close the modal

  }



  $ionicModal.fromTemplateUrl('client/templates/transferMoney.html', {
    scope: $scope,
    animation: 'slide-in-up',
  }).then(function(modal) {
    $scope.transferMoneyModal = modal;
  });

  $scope.openTransferMoneyModal = function() {
    $scope.transferMoneyModal.show();
  };

  $scope.closeTransferMoneyModal = function() {
    $scope.transferMoneyModal.hide();
  };

  //Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.transferMoneyModal.remove();
  });





  // ADD CARD ------------------------------------------------------

  $ionicModal.fromTemplateUrl('client/templates/addCard.html', {
    scope: $scope,
    animation: 'slide-in-up',
  }).then(function(modal) {
    $scope.addCardModal = modal;
  });

  $scope.openAddCardModal = function() {
    $scope.addCardModal.show();
  };

  $scope.closeAddCardModal = function() {
    $scope.addCardModal.hide();
  };

  //Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.addCardModal.remove();
  });


  $scope.addCard = function(addCardForm) {
    console.log(" --- addCard called ---");


    ccNum = $('#credit_card_number').val();
    cvc = $('#cvv_number').val();
    expMo = $('#month_expire').val();
    expYr = $('#year_expire').val();

    console.log(ccNum + cvc + expMo + expYr);

    let cardObject = {
      number: ccNum,
      cvc: cvc,
      exp_month: expMo,
      exp_year: expYr
    }

    //add the card info to the user's profile
    console.log("Now save this:");
    console.log(cardObject);
    Meteor.call('saveCardInfo', cardObject);

    //   Stripe.card.createToken({
    //     number: ccNum,
    //     cvc: cvc,
    //     exp_month: expMo,
    //     exp_year: expYr,
    // }, function(status, response) {
    //     stripeToken = response.id;
    //     console.log('stripeToken');
    //     console.log(stripeToken);
    //     // Meteor.call('chargeCard', stripeToken);
    // });

    $scope.closeAddCardModal(); //close the modal
  }


  // EDIT CARD ------------------------------------------------------
  $scope.cardModel; //holds the card info

  $ionicModal.fromTemplateUrl('client/templates/editCard.html', {
    scope: $scope,
    animation: 'slide-in-up',
  }).then(function(modal) {
    $scope.editCardModal = modal;
  });

  $scope.openEditCardModal = function(card) {
    //create the model and set variables
    console.log("openEditCardModal called");
    console.log(card);

    $scope.cardModel = {
      // card identifiers
      oldNumber: card.number,
      oldCvc: card.cvc,
      // fields for editing:
      number: card.number,
      cvc: card.cvc,
      exp_month: card.exp_month,
      exp_year: card.exp_year
    };

    $scope.editCardModal.show();
  };

  $scope.closeEditCardModal = function() {
    $scope.editCardModal.hide();
  };

  //Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.editCardModal.remove();
  });


  $scope.updateCardInfo = function() {
    console.log(" --- updateCardInfo called ---");
    //add the card info to the user's profile
    // console.log("Now save this:");
    // console.log($scope.cardModel);
    Meteor.call('updateCardInfo', $scope.cardModel);

    // Do something with Stripe here?


    //   Stripe.card.createToken({
    //     number: ccNum,
    //     cvc: cvc,
    //     exp_month: expMo,
    //     exp_year: expYr,
    // }, function(status, response) {
    //     stripeToken = response.id;
    //     console.log('stripeToken');
    //     console.log(stripeToken);
    //     // Meteor.call('chargeCard', stripeToken);
    // });

    $scope.closeEditCardModal(); //close the modal
  }


  // ADD BANK ----------------------------------------------------------------

  $ionicModal.fromTemplateUrl('client/templates/addBank.html', {
    scope: $scope,
    animation: 'slide-in-up',
  }).then(function(modal) {
    $scope.addBankModal = modal;
  });

  $scope.openAddBankModal = function() {
    $scope.addBankModal.show();
  };

  $scope.closeAddBankModal = function() {
    $scope.addBankModal.hide();
  };

  //Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.addBankModal.remove();
  });


  $scope.addBank = function() {
    console.log(" --- addBank called ---");

    // addBank logic goes here
    // ...
    // ...
    // ...
    // ...

    $scope.closeAddBankModal(); //close the modal
  }


  // EDIT BANK ------------------------------------------------------
  $scope.bankModel; //holds the bank info

  $ionicModal.fromTemplateUrl('client/templates/editBank.html', {
    scope: $scope,
    animation: 'slide-in-up',
  }).then(function(modal) {
    $scope.editBankModal = modal;
  });

  $scope.openEditBankModal = function(bank) {
    //create the model and set variables
    console.log("openEditBankModal called");
    console.log(bank);

    $scope.bankModel = {
      // bank identifiers

      // fields for editing:

    };

    $scope.editBankModal.show();
  };

  $scope.closeEditBankModal = function() {
    $scope.editBankModal.hide();
  };

  //Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.editBankModal.remove();
  });


  $scope.updateBankInfo = function() {
    console.log(" --- updateBankInfo called ---");
    //add the card info to the user's profile
    // console.log("Now save this:");
    // console.log($scope.cardModel);

    // Not defined yet:
    // Meteor.call('updateBankInfo', $scope.cardModel);

    // Do something with Stripe here?


    $scope.closeEditBankModal(); //close the modal
  }



  // PAYMENT HISTORY  ------------------------------------------------------

  $ionicModal.fromTemplateUrl('client/templates/paymentHistory.html', {
    scope: $scope,
    animation: 'slide-in-up',
  }).then(function(modal) {
    $scope.paymentHistoryModal = modal;
  });

  $scope.openPaymentHistoryModal = function(bank) {

    $scope.paymentHistoryModal.show();
  };

  $scope.closePaymentHistoryModal = function() {
    $scope.paymentHistoryModal.hide();
  };

  //Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.paymentHistoryModal.remove();
  });


}

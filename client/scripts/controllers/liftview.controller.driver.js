// DRIVER-specific logic

angular.expandControllerLiftViewDriver = function($scope, $reactive, $stateParams, $meteor, $ionicModal, $timeout, $log, $state, $controller, $location, $ionicActionSheet, popupController, liftStateService, uiGmapGoogleMapApi){


  $scope.deleteLiftButton = function(liftData) {
    popupController.showConfirm("Woah there!", "Are you sure you want to delete this lift?", function(confirm){
      if(confirm){ //If they say OK,
        $location.path("/"); // redirect to home
        Meteor.call('deleteLift', liftData);// and delete the lift.
      }
    }, "Yeah", "Nope");
  };


  ////////////////////////////////////
  //////// CHECKPOINT METHODS  ///////
  ////////////////////////////////////

  $scope.startLiftCheckpoint = function() {
    // let checkpoint = {
    //   timeStamp: new Date();
    //   location: null; //just for now - NEED TO GET THE USER's REAL LOCATION
    //   type: "startLift";
    // }
    // console.log("Checkpoint object:");
    // console.log(checkpoint);

    popupController.showConfirm("Start this lift?", "This will notify your riders that you're on your way.", function(confirm){
          if(confirm){ //If they say OK,

            $scope.isActive = true;
            $scope.driverEnRoute = true;

            Meteor.call('updateLiftState', $stateParams.liftId, "isActive", true);
            Meteor.call('updateLiftState', $stateParams.liftId, "driverEnRoute", true);

            Meteor.call('startLiftCheckpointNotifications', $stateParams.liftId);

          }
        }, "Start", "Cancel");

        //This shouldn't go here, but apparentyl popupControllers CAN NOT BE NESTED!!!
        // popupController.showAlert("You're lift has started!", "Make sure to tap 'End Lift' when you arrive at your destination.", function(){});





  };

    
  $scope.arrivedAtDestinationCheckpoint = function() {
    // let checkpoint = {
    //   timeStamp: new Date();
    //   location: null; //just for now - NEED TO GET THE USER's REAL LOCATION
    //   type: "startLift";
    // }
    // console.log("Checkpoint object:");
    // console.log(checkpoint);

    Meteor.call('updateLiftState', $stateParams.liftId, "hasArrivedAtDestination", true);
    Meteor.call('updateLiftState', $stateParams.liftId, "isComplete", true);

    $scope.hasArrivedAtDestination = true;
    $scope.isComplete = true;



    //TEMP FIX
    // if(Meteor.userId() == $scope.testLift.driverId){ // if the user is the DRIVER:
    // }
      $scope.openRating();
    

    // Activate ratings / payments screen
  };

  $ionicModal.fromTemplateUrl('client/templates/rateUser.html', {
      scope: $scope,
      animation: 'slide-in-up',
    }).then(function (modal) {
      $scope.rateUser = modal;
    });

    $scope.openRating = function(){
      $scope.rateUser.show();
    }

    $scope.closeRating = function(){
      $scope.rateUser.hide();
    }

  $scope.openContactRiderActionSheet = function(phoneNumber) {

    if(phoneNumber){
      let num = String(phoneNumber).replace(/\D/g,''); // Regex to strip out non-numerical values
      num = num.replace(/(\d{3})(\d{3})(\d{4})/, "$1-$2-$3"); // Super duper regex to put dashes back in
      let callHook = "tel:+1-" + num;
      let textHook = "sms:+1-" + num;

      // Show the action sheet:
      $ionicActionSheet.show({
        buttons: [{
          text: 'Call Rider'
        }, {
          text: 'Text Rider'
        }],
        cancelText: 'Cancel',
        buttonClicked: function(index, buttonObj) {
         switch (index) {
           case 0:
           //Call the number
            window.location.href = callHook;
             return false;
           case 1:
           //text the number
               window.location.href = textHook;
             return false;
         }
        }
      });
    }
    else{
      alert("Something went wrong... the driver doesn't have a phone number on file.");
    }


 };

 $scope.confirmRiderPickup = function(liftId, riderObject){
   console.log("== confirmRiderPickup called");
   console.log(liftId);
   console.log(riderObject);

   //meteor method to change the value of pickupDropOffConfirmed inside the
   Meteor.call('confirmRiderPickupDropoff', liftId, riderObject);

 };

 $scope.confirmRiderDropOff = function(liftId, riderObject){
  //  console.log("== confirmRiderDropOff called");
  //  console.log(liftId);
  //  console.log(riderObject);

   Meteor.call('confirmRiderPickupDropoff', liftId, riderObject);

 };


 ////////////////////////////////////
 ///////////// Getters  /////////////
 ////////////////////////////////////

 $scope.getGoogleRedirectURL = function(riderAddress){
   let concatenatedAddress = riderAddress.replace(/\s/g,'+'); //replace every space with a + sign
  //  console.log(concatenatedAddress);
   let redirectURL = "https://maps.google.com/?q=" + concatenatedAddress; //add the concatenated address on to the redirect URL
  //  console.log(redirectURL);
   return redirectURL;
 };


 uiGmapGoogleMapApi.then(function(maps) {
            console.log('THIS IS THE TEST LIFT')
            console.log($scope.testLift)
            $scope.points = []
            populatePointsArray();

            //     {latitude: $scope.testLift.startEndLocation.lat,
            //     longitude: $scope.testLift.startEndLocation.lat,
            //     idKey: 1,
            //     icon: 'StartLocation.png'
            //     },
            //     {latitude: $scope.testLift.endLocation.lat,
            //     longitude: $scope.testLift.endLocation.long,
            //     idKey: 2,
            //     icon: 'Destination.png'
            //     }
            // ]

            function populatePointsArray(){
              $scope.points.push({
                coords: {
                  latitude: $scope.testLift.startEndLocation.lat,
                  longitude: $scope.testLift.startEndLocation.long,
                },
                id: 1,
                title: 'Start Location',
                options: {
                  icon: 'StartLocation.png'
                }
                }
              )

              $scope.points.push({
                coords: {
                  latitude: $scope.testLift.endLocation.lat,
                  longitude: $scope.testLift.endLocation.long,
                },
                id: 2,
                options: {
                  icon: 'Destination.png'
                },
                title: 'Destination'
                }
              )

              for(var i = 0; i < $scope.testLift.potentialRiders.length; i++){
                console.log($scope.testLift)
                  $scope.points.push({
                    coords: {
                      latitude: $scope.testLift.potentialRiders[i].pickupDropoffLocation.lat,
                      longitude: $scope.testLift.potentialRiders[i].pickupDropoffLocation.long,
                    },
                    id: i + 3,
                    options: {
                      icon: 'PassengerPickup.png'
                    },
                    title: $scope.testLift.potentialRiders[i].userName
                  })
              }

              for(var i = 0; i < $scope.testLift.confirmedRiders.length; i++){
                console.log($scope.testLift)
                  $scope.points.push({
                    coords: {
                      latitude: $scope.testLift.confirmedRiders[i].pickupDropoffLocation.lat,
                      longitude: $scope.testLift.confirmedRiders[i].pickupDropoffLocation.long,
                    },
                    id: i + 3 + $scope.testLift.potentialRiders.length,
                    options: {
                      icon: 'PassengerPickup.png'
                    },
                    title: $scope.testLift.confirmedRiders[i].userName
                  })
              }
            }


            console.log('HERE ARE THE POINTS')
            console.log($scope.points)


            $scope.map = {center: {latitude: $scope.testLift.startEndLocation.lat, longitude: $scope.testLift.startEndLocation.long }, zoom: 10 };

            // var bounds = new google.maps.LatLngBounds();
            // for (var i = 0; i < $scope.points.length; i++) {
            // bounds.extend({lat: $scope.points[i].latitude, lng: $scope.points[i].longitude});
            // }

            // $scope.map.fitBounds(bounds);

            var styleArray = [
                    {"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#444444"}]},
                    {"featureType":"landscape","elementType":"all","stylers":[{"color":"#f2f2f2"}]},
                    {"featureType":"poi","elementType":"all","stylers":[{"visibility":"off"}]},
                    {"featureType":"road","elementType":"all","stylers":[{"saturation":-100},{"lightness":45}]},
                    {"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},
                    {"featureType":"road.arterial","elementType":"labels.icon","stylers":[{"visibility":"off"}]},
                    {"featureType":"transit","elementType":"all","stylers":[{"visibility":"off"}]},
                    {"featureType":"water","elementType":"all","stylers":[{"color":"#46bcec"},{"visibility":"on"}]}
                ]
            $scope.options = {
                scrollwheel: false,
                styles: styleArray,
                disableDefaultUI: true,
            };


        });



}

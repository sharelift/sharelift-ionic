angular
  .module('Sharelift')
  .config(config);

function config($stateProvider, $urlRouterProvider, uiGmapGoogleMapApiProvider, $ionicConfigProvider) {

  uiGmapGoogleMapApiProvider.configure({
      key: 'AIzaSyBjUTnBj0wLepey0FipdJgbo0Q7s_2fvHs',
      v: '3.20',
      libraries: 'weather,geometry,visualization,places'
  });

  $ionicConfigProvider.tabs.position('bottom');


  $stateProvider
    .state('Main', {
      url: "/index",
      abstract: true,
      templateUrl: "client/templates/main.html",
      resolve: {
        "currentUser": function($meteor) {  //check that the user is logged in
          return $meteor.requireUser();
        },
        getUser() {
          return Meteor.user();
        },
        getLifts() {
          return Meteor.subscribe('getOpenLifts');
        },
      }
    })
    .state('Main.ride', {
      url: "/ride",
      views: {
        'ride-tab': {
          templateUrl: "client/templates/tabs/ride.html",
          controller: 'MainCtrl as main'
        }
      }
    })
    .state('Main.myLifts', {
      url: "/myLifts",
      views: {
        'my-lifts-tab': {
          templateUrl: "client/templates/tabs/myLifts.html",
          controller: 'MainCtrl as main'
        }
      }
    })
    .state('Main.drive', {
      url: "/drive",
      views: {
        'drive-tab': {
          templateUrl: "client/templates/tabs/drive.html",
          controller: 'MainCtrl as main'
        }
      }
    })
    .state('Main.profile', {
      url: '/profile',
      views: {
        'profile-tab': {
          templateUrl: 'client/templates/profile.html',
          controller: 'MainCtrl as main'
        }
      }
    })
    .state('Main.notif', {
      url: '/notifications',
      views: {
        'notif-tab': {
          templateUrl: 'client/templates/notifications.html',
          controller: 'MainCtrl as main'
        }
      }
    })


    // .state('Main', {
    //   url: '/index',
    //   abstract: false,
    //   templateUrl: 'client/templates/main.html',
    //   controller: 'MainCtrl as main',
    //   resolve: {
    //     "currentUser": function($meteor) {  //check that the user is logged in
    //       return $meteor.requireUser();
    //     },
    //     getUser() {
    //       return Meteor.user();
    //     },
    //     getLifts() {
    //       return Meteor.subscribe('getOpenLifts');
    //     },
    //   }
    // })

// State changes from RIDE TAB

    .state('Main.lift', {
      url: '/lift/:liftId',
      views: {
        'ride-tab': {
          templateUrl: 'client/templates/liftView.html',
          controller: 'LiftViewCtrl as lift'
        }
      },
      resolve : {
        "currentUser": function($meteor) { //check that the user is logged in
          return $meteor.requireUser();
        },
        liftId : ['$stateParams', function($stateParams){
          console.log("ayo we in hurr" + $stateParams.liftId);
          return $stateParams.liftId;
        }]
      }
    })
    .state('Main.liftDriver', {
      url: '/liftDriver/:liftId',
      views: {
        'ride-tab': {
          templateUrl: 'client/templates/liftViewDriver.html',
          controller: 'LiftViewCtrl as lift'
        }
      },
      resolve : {
        "currentUser": function($meteor) { //check that the user is logged in
          return $meteor.requireUser();
        },
        liftId : ['$stateParams', function($stateParams){
          console.log("ayo we in hurr" + $stateParams.liftId);
          return $stateParams.liftId;
        }]
      }
    })

// State changes from MY LIFTS TAB
    .state('Main.myLift', {
      url: '/myLift/:liftId',
      views: {
        'my-lifts-tab': {
          templateUrl: 'client/templates/liftView.html',
          controller: 'LiftViewCtrl as lift'
        }
      },
      resolve : {
        "currentUser": function($meteor) { //check that the user is logged in
          return $meteor.requireUser();
        },
        liftId : ['$stateParams', function($stateParams){
          console.log("ayo we in hurr" + $stateParams.liftId);
          return $stateParams.liftId;
        }]
      }
    })
    .state('Main.myLiftDriver', {
      url: '/myLiftDriver/:liftId',
      views: {
        'my-lifts-tab': {
          templateUrl: 'client/templates/liftViewDriver.html',
          controller: 'LiftViewCtrl as lift'
        }
      },
      resolve : {
        "currentUser": function($meteor) { //check that the user is logged in
          return $meteor.requireUser();
        },
        liftId : ['$stateParams', function($stateParams){
          console.log("ayo we in hurr" + $stateParams.liftId);
          return $stateParams.liftId;
        }]
      }
    })

    .state('Main.myLiftRequest', {
      url: '/myRequest/:requestId',
      views: {
        'my-lifts-tab': {
          templateUrl: 'client/templates/requestViewRider.html',
          controller: 'RequestViewCtrl as request'
        }
      },
      resolve : {
        "currentUser": function($meteor) { //check that the user is logged in
          return $meteor.requireUser();
        },
        requestId : ['$stateParams', function($stateParams){
          console.log("ayo we in hurr" + $stateParams.requestId);
          return $stateParams.requestId;
        }]
      }
    })

    .state('Main.notifRequest', {
      url: '/notifRequest/:requestId',
      views: {
        'notif-tab': {
          templateUrl: 'client/templates/requestViewRider.html',
          controller: 'RequestViewCtrl as request'
        }
      },
      resolve : {
        "currentUser": function($meteor) { //check that the user is logged in
          return $meteor.requireUser();
        },
        requestId : ['$stateParams', function($stateParams){
          console.log("ayo we in hurr" + $stateParams.requestId);
          return $stateParams.requestId;
        }]
      }
    })

    .state('Main.request', {
      url: '/request/:requestId',
      views: {
        'drive-tab': {
          templateUrl: 'client/templates/requestViewRider.html',
          controller: 'RequestViewCtrl as request'
        }
      },
      resolve : {
        "currentUser": function($meteor) { //check that the user is logged in
          return $meteor.requireUser();
        },
        requestId : ['$stateParams', function($stateParams){
          console.log("ayo we in hurr" + $stateParams.requestId);
          return $stateParams.requestId;
        }]
      }
    })
    .state('Main.offer', {
      url: '/offer/:requestId',
      views: {
        'drive-tab': {
          templateUrl: 'client/templates/requestView.html',
          controller: 'RequestViewCtrl as request'
        }
      },
      resolve : {
        "currentUser": function($meteor) { //check that the user is logged in
          return $meteor.requireUser();
        },
        requestId : ['$stateParams', function($stateParams){
          console.log("ayo we in hurr" + $stateParams.requestId);
          return $stateParams.requestId;
        }]
      }
    })

    // .state('notifications', {
    //   url: '/notifications',
    //   templateUrl: 'client/templates/partials/notifications.html',
    //   controller: 'MainCtrl as main',
    //   resolve: {
    //     "currentUser": function($meteor) { //check that the user is logged in
    //       return $meteor.requireUser();
    //     },
    //     getProfile($stateParams) {
    //       return Meteor.subscribe('getPublicProfileData', $stateParams.userId);
    //     }
    //   }
    // })


    .state('Main.publicProfile', {
      url: '/publicProfile/:userId',
      views: {
        'ride-tab': {
          templateUrl: 'client/templates/publicProfile.html',
          controller: 'ProfileCtrl as profile'
        },
        'my-lifts-tab': {
          templateUrl: 'client/templates/publicProfile.html',
          controller: 'ProfileCtrl as profile'
        },
        'drive-tab': {
          templateUrl: 'client/templates/publicProfile.html',
          controller: 'ProfileCtrl as profile'
        }

      },
      // templateUrl: 'client/templates/publicProfile.html',
      // controller: 'ProfileCtrl as profile',
      resolve: {
        "currentUser": function($meteor) { //check that the user is logged in
          return $meteor.requireUser();
        },
        userId : ['$stateParams', function($stateParams){
          console.log("ayo we in hurr" + $stateParams.userId);
          return $stateParams.userId;
        }]
        // getProfile($stateParams) {
        //   return Meteor.subscribe('getPublicProfileData', $stateParams.userId);
        // }
      }
    })
    // All login screens
    .state('login', {
      url: '/login',
      // loaded into ui-view of parent's template
      templateUrl: 'client/templates/onboarding/login.html',
      controller : 'LoginCtrl'
    })
    .state('loginDetail', {
      url: '/loginDetail',
      // loaded into ui-view of parent's template
      templateUrl: 'client/templates/onboarding/loginDetail.html',
      controller : 'LoginCtrl'
    })
    .state('createAccount', {
      url: '/createAccount',
      // loaded into ui-view of parent's template
      templateUrl: 'client/templates/onboarding/createAccount.html',
      controller : 'LoginCtrl'
    })

    // From loginDetail.html
    .state('forgotPassword', {
      url: '/forgotPassword',
      // loaded into ui-view of parent's template
      templateUrl: 'client/templates/onboarding/forgotPassword.html',
      controller : 'LoginCtrl'
    })

    // On-boarding screens
    .state('addFBInfo', {
      url: '/addFBInfo',
      // loaded into ui-view of parent's template
      templateUrl: 'client/templates/onboarding/addFBInfo.html',
      controller : 'LoginCtrl',
      resolve: {
        "currentUser": function($meteor) {  //check that the user is logged in
          return $meteor.requireUser();
        }
      }
    })
    .state('inputProfileSettings1', {
      url: '/inputProfileSettings1',
      // loaded into ui-view of parent's template
      templateUrl: 'client/templates/onboarding/inputProfileSettings1.html',
      controller : 'LoginCtrl',
      resolve: {
        "currentUser": function($meteor) {  //check that the user is logged in
          return $meteor.requireUser();
        }
      }
    })
    .state('inputProfileSettings2', {
      url: '/inputProfileSettings2',
      // loaded into ui-view of parent's template
      templateUrl: 'client/templates/onboarding/inputProfileSettings2.html',
      controller : 'LoginCtrl',
      resolve: {
        "currentUser": function($meteor) {  //check that the user is logged in
          return $meteor.requireUser();
        }
      }
    })
    .state('phoneInput', {
      url: '/phone-input',
      // loaded into ui-view of parent's template
      templateUrl: 'client/templates/onboarding/phoneInput.html',
      controller : 'LoginCtrl',
      resolve: {
        "currentUser": function($meteor) {  //check that the user is logged in
          return $meteor.requireUser();
        }
      }
    })
    .state('driverSignup', {
      url: '/driverSignup',
      // loaded into ui-view of parent's template
      templateUrl: 'client/templates/driverSignup.html',
      controller : 'DriverSignupCtrl',
      resolve: {
        "currentUser": function($meteor) {  //check that the user is logged in
          return $meteor.requireUser();
        }
      }
    })
    .state('profilePayment', {
      url: '/profile-payment',
      // loaded into ui-view of parent's template
      templateUrl: 'client/templates/profile-payment.html',
      controller : 'ProfilePaymentCtrl as profilePayment',
      resolve: {
        "currentUser": function($meteor) {  //check that the user is logged in
          return $meteor.requireUser();
        }
      }
    });



  $urlRouterProvider.otherwise('/index/myLifts');
}

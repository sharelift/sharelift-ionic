angular
  .module('Sharelift')
  .filter('formatLiftDepartureTime', formatLiftDepartureTime);

angular
  .module('Sharelift')
  .filter('liftName', liftName);

angular
  .module('Sharelift')
  .filter('liftPhotoFilter', liftPhotoFilter);

angular
  .module('Sharelift')
  .filter('riderPhotoFilter', riderPhotoFilter);

angular
  .module('Sharelift')
  .filter('requestPhotoFilter', requestPhotoFilter);

angular
  .module('Sharelift')
  .filter('timeDateFilter', timeDateFilter);

angular
  .module('Sharelift')
  .filter('timeDateFilterWithPostfix', timeDateFilterWithPostfix);

angular
  .module('Sharelift')
  .filter('formatPhoneFilter', formatPhoneFilter);

angular
  .module('Sharelift')
  .filter('liftListFilter', liftListFilter);

angular
  .module('Sharelift')
  .filter('requestListFilter', requestListFilter);

angular
  .module('Sharelift')
  .filter('range', range);




function formatLiftDepartureTime() {
  return function (lift) {
    if (!lift) return;
    let time = moment(lift.departureTime);
    let str = time.format('MM/DD @ H:mm');
    return str;
  };
}

function liftPhotoFilter(){
  return function(lift){
    let ret = "/profile_filler.png.png"
    let driver = "";
    if(lift){
      if(lift.driverId){
        driver = Meteor.users.findOne({_id : lift.driverId});
      }
      if(driver){
        if(driver.profile.profilePicture){
          ret = driver.profile.profilePicture;
        }
      }
    }
    return ret;
  };
}

function requestPhotoFilter(){
  return function(request){
    let ret = "/profile_filler.png.png"
    let driver = "";
    if(request){
      if(request.riderId){
        rider = Meteor.users.findOne({_id : request.riderId});
      }
      if(rider){
        if(rider.profile.profilePicture){
          ret = rider.profile.profilePicture;
        }
      }
    }
    return ret;
  };
}

function riderPhotoFilter(){
  return function(rider){
    let ret = "/profile_filler.png.png"
    //console.log(rider)
    let riderProfile = Meteor.users.findOne({ _id : rider.userId});
    //console.log(riderProfile);
    if(riderProfile && riderProfile.profile && riderProfile.profile.profilePicture){
      ret = riderProfile.profile.profilePicture;
    }
    return ret;
  };
}

function formatPhoneFilter(){
  return function(number){
    let num = String(number).replace(/\D/g,''); // Regex to strip out non-numerical values
    num = num.replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2-$3"); // Super duper regex to put dashes back in
    return num;
  };

}

function liftName() {
  return function (id) {
    if (!id) return;
    var dst = "";
    if(id == 1) {
      dst = "Big Sky Resort";
    } else {
      dst = "Bridger Bowl";
    }

    //works but errors a shitload and is kind of slow...?
    // return lift.driverName + " - " + Locations.findOne({locationId : lift.destinationLocationId}).destinationName;

    // return lift.driverName + " - " + dst;
    return dst;
  };
}

function timeDateFilter(){
  return function(td_string){
    return timeDateFilterBase(td_string, false);
  }

}

function timeDateFilterWithPostfix(){
  return function(td_string){
    return timeDateFilterBase(td_string, true);
  }


}

function timeDateFilterBase(td_string, postfix_bool){
  let ret = "";
  let d = new Date(td_string);
  let d_hour = d.getHours();
  let d_min = d.getMinutes();
  let d_offset = d.getTimezoneOffset()/60;
  let d_postfix = "a";
  if(d_hour > 12){
    d_hour = d_hour - 12;
    d_postfix = "p";
  }
  d_min = ("0" + String(d_min)).slice(-2);
  if(postfix_bool){
    ret = String(d_hour) + ":" + String(d_min) + " " + d_postfix;
  }
  else{
    ret = String(d_hour) + ":" + String(d_min);
  }
  return ret;

}

//a custom filter for the lifts displayed in the lift list
function liftListFilter() {
  return function (lifts, date) {

    if(!date){  //if date == null
      return lifts; //return all lifts
    }
    else{ // if a date has been selected, filter for lifts on that day
      dateString = date.toDateString(); //convert the filter date to a string for easy comparison
      var result = [];
      for (var i=0; i<lifts.length; i++){
        if(dateString == lifts[i].departureTime.toDateString()){
          result.push(lifts[i]);
        }
      }
      return result;
    }

  };
}

function range() {
  return function(input, total) {
    total = parseInt(total);

    for (var i=0; i<total; i++) {
      input.push(i);
    }

    return input;
  };
}

function requestListFilter() {
  return function (requests, date) {

    if(!date){  //if date == null
      return requests; //return all lifts
    }
    else{ // if a date has been selected, filter for lifts on that day
      dateString = date.toDateString(); //convert the filter date to a string for easy comparison
      var result = [];
      for (var i=0; i<requests.length; i++){
        if(dateString == requests[i].startDepartureTime.toDateString()){
          result.push(requests[i]);
        }
      }
      return result;
    }
  };
}

//   $scope.filter("myfilter", function() {
//   return function(lifts, date) {
//     console.log("--- FILTER CALLED --- ");
//     console.log(date);
//         var result = [];
//         for (var i=0; i<lifts.length; i++){
//                 result.push(lifts[i]);
//         }
//         return result;
//   };
// });

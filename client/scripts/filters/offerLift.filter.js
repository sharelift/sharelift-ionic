angular
  .module('Sharelift')
  .filter('formatOfferLiftDate', formatOfferLiftDate);


function formatOfferLiftDate() {
  return function (days) {
    return moment().add(days, 'days').format('dddd, MMMM Do'); // "Sunday, February 14th or something
  };
}

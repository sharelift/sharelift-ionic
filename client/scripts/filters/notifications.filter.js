angular
  .module('Sharelift')
  .filter('notificationText', notificationText);

function notificationText () {
  return function (notif) {
    if (!notif) return;
    let notifBody;
    let notifType = notif.notifType;
    if(notifType == "denyLift"){
      notifBody = notif.userName + " denied your request.";
    }
    else if(notifType == "confirmLift"){
      notifBody = notif.userName + " confirmed your request."
    }
    else if(notifType == "requestLift"){
      notifBody = notif.userName + " requested your lift."
    }
    else if(notifType =="canceledRider"){
      notifBody = notif.notifText;
      if(!notif.notifText){
        notifBody = notif.userName + " just canceled their lift with you.";
      }
    }
    else if(notifType == "confirmOffer"){
      notifBody = notif.userName + " has accepted your offer."
    }
    else if(notifType == "sendOffer"){
      notifBody = notif.userName + " has offered you a lift."
    }
    else{
      notifBody = notif.notifText;
    }
    return notifBody;
  };
}

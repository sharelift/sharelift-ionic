angular
  .module('Sharelift')
  .filter('profileAsdf', profileAsdf);

angular
  .module('Sharelift')
  .filter('profileName', profileName);

angular
  .module('Sharelift')
  .filter('profilePhotoFilter', profilePhotoFilter);

function profileAsdf () {
  return function (time) {
    if (!time) return;

    return "rargh";
  };
}

function profileName(){
  return function(name){
    let ret = "User not logged in";
    if(name){
      let n = name.split(" ");
      let lastName = n[n.length - 1];
      let lastInitial = lastName.charAt(0);
      let lastIndex = name.lastIndexOf(" ");

      let firstName = name.substring(0, lastIndex);
      ret = firstName + " " + lastInitial + ".";
    }
    return ret;
  }
}

function profilePhotoFilter(){
  return function(photo){
    if(!photo){
      return "/profile_filler.png.png";
    }
    return photo;
  };
}

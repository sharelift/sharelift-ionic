angular
  .module('Sharelift', [
    'angular-meteor',
    'ionic',
    'angularMoment',
    'ngMessages',
    'ion-datetime-picker',
    'ionic.rating',
    'google.places',
    'ng-mfb',
    'ngCordova',
    'uiGmapgoogle-maps',
    'ion-google-place',
    //'Stripe'
  ])
  .run(function($ionicPickerI18n) {
        $ionicPickerI18n = {
          'am-pm': true,
        }


    })

Meteor.startup(function() {
    
    Stripe.setPublishableKey('pk_test_uwaHR1qvD1LUJnhZzGje4Hr4');
    var handler = StripeCheckout.configure({
		key: 'pk_test_uwaHR1qvD1LUJnhZzGje4Hr4',
		token: function(token) {}
	});
    
    Session.set("terms&conditions", localStorage.getItem("terms&conditions"));
    if(Session.get("terms&conditions") == null){
      console.log('Object doesn\'t exist, setting now')
      localStorage.setItem("terms&conditions", "true");
      Session.set("terms&conditions", localStorage.getItem("terms&conditions"));
    } else {
      console.log('Object exists!')
    }
  })

  //"ionic-timepicker": "latest", for bower.json
  //'ionic-timepicker', For this file

if (Meteor.isCordova) {
  angular.element(document).on('deviceready', onReady);
}
else {
  angular.element(document).ready(onReady);
}

function onReady() {
  angular.bootstrap(document, ['Sharelift']);
  setTimeout(function(){ window.loading_screen.finish(); }, 2000);
}
